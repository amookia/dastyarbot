from saham.saham import sahamtool
from threading import Thread
import schedule
import time


schedule.every(15).minutes.do(sahamtool.repeater)
schedule.every(2).hours.do(sahamtool.stock_update)

while True:
    schedule.run_pending()
    time.sleep(1)
