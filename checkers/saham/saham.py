import requests
from src.models import db


class sahamtool:
    def repeater(self):
        print('start')
        try:
            url = 'http://boursezone.ir/api/?token=ac3ed71d929300f45b29445f5a844632&all'
            stocks_list = requests.get(url).json()
            for stock in stocks_list:
                isin = stock['namad_code']
                db.stockscache.find_one_and_update({'namad_code':isin},{'$set':stock},upsert=True)
        except Exception as e:
            print(e)

    def stock_update(self):
        url = 'https://core.tadbirrlc.com//StocksHandler.ashx?{%22Type%22:%22ALL21%22%2C%22la%22:%22Fa%22}&jsoncallback='
        try:
            all_stocks = requests.get(url).json()
            for stock in all_stocks:
                isin = stock['nc']
                name = stock['sf']
                find = db.stocks.count({'isin':isin})
                if find == 0:
                    db.stocks.insert_one({'name':name,'isin':isin})
        except Exception as e:
            print(e)


sahamtool = sahamtool()
