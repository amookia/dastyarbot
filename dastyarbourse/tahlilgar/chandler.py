from tahlilgar.codal import codal

def chandler(chatid,text):
    split = text.split('|')
    text = split[0]
    name = split[1]
    page = int(split[2])


    if text == 'codal_financestate':
        codal().financestate(chatid,name,page)

    if text == 'codal_monthly_report':
        codal().monthly_report(chatid,name,page)

    if text == 'codal_benefit':
        codal().benefit(chatid,name,page)

    if text == 'codal_total':
        codal().total(chatid,name,page)

    if text == 'codal_info':
        codal().info(chatid,name,page)

    if text== 'codal_price_increase':
        codal().price_increase(chatid,name,page)
