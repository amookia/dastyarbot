import matplotlib
import matplotlib.pyplot as plt
import arabic_reshaper
import io
import requests
from bidi.algorithm import get_display

matplotlib.use('Agg')


def make_farsi_text(x):
    reshaped_text = arabic_reshaper.reshape(x)
    farsi_text = get_display(reshaped_text)
    return farsi_text

def get_image(name):
    url = ' http://sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&all_indicators&name={}'.format(name)
    res = requests.get(url).json()
    data = res['technical_sum']
    labels = make_farsi_text("خرید"), make_farsi_text("فروش"), make_farsi_text("بی طرف")
    sizes = [data['buy'], data['sell'], data['neutral']]
    fig1, ax1 = plt.subplots()
    ax1.bar(labels[0],sizes[0],color="#2cc0d5",label = labels[0])
    ax1.bar(labels[1],sizes[1],color="#FF4500",label = labels[1])
    ax1.bar(labels[2],sizes[2],color="#38374A",label = labels[2])
    for label in ax1.xaxis.get_ticklabels():
        label.set_fontsize(16)
    plt.title(make_farsi_text(name),fontsize=16)
    plt.xlabel('\n@dastyarboursebot')
    plt.legend()
    ax1.set(xlabel='\n@dastyarboursebot')
    fig1.set_size_inches(10.5, 10.5)
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)
    plt.close(fig1)
    return buf.getvalue()
