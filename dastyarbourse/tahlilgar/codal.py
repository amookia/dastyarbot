from telethon.sync import TelegramClient
from telethon.sessions import StringSession
from telethon.tl.types import InputMessagesFilterPhotos
from src.models import db
import requests
import urllib.parse
import re
import asyncio
import json
import config

api_id = 1283044
api_hash = 'c26de812e6d56336b4d06968083ea06b'




class codal:
    def __init__(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        #self.token = '1294846811:AAG3NKmiGY4rrrsTs6raJ0m2ARomS1IZXLg'
        self.token = config.token
        self.api_url = 'https://api.telegram.org/' + self.token + '/'
        self.session = '1BJWap1sBu3ROBafsOCZ1AMKaE-MHZvSepqR3fWF3l7-VFBXHwxKI8qYSv5xNl4wSZZPYOtm5_4HgX9NLPErAX74N3N0uiv4mlB3fLG5KGFCWPoFTc9HKik3QdRX-Mb88oDHhcGE7GJ3FFcCxKVGZweC9c5AWF6aT5DkAdMKkhQ-fnQrOCObQTTrRO9mKWqNRXYXnZNmGUe-Y3x81hvZv3HXvLuuLW1tTN-el6AV-LzjwdaFw-_qxg19WiIF3wHkcGf7t_w6L-shPGOH9evUORPdyEVnGDVGdTH9b1WPNTDag35k6BWyrlxEr8Uz2Gta3TgXhMdyD0OtDl7q1xBQiXIBmlAeZJ5M='


    def crawler(self,chatid,sahm_name,lists,page,stat):
        with TelegramClient(StringSession(self.session),api_id,api_hash) as client:

            sahm = f'#{sahm_name}'
            search = client.get_messages(-1001355843251,None,search=sahm,filter=InputMessagesFilterPhotos)
            total = 0
            found = 0
            #print(f'start {(page * 5) - 5}| finish : {page * 5}')
            start = (page * 5) -5
            finish = page * 5
            for message in search:
                for qu in lists:
                    if qu in message.text:
                        if start <= total <= finish :
                            text = urllib.parse.quote(message.message.replace('➖➖➖➖⬇️⬇️⬇️⬇️','').replace('@Codal360_ir',''))
                            #send photo
                            data = client.download_media(message,file=bytes)
                            text += '\n@DastyarBoursebot'
                            files = {'photo': data }
                            url =  self.api_url + 'sendPhoto?chat_id=' + str(chatid) + '&caption=' + text + \
                            '&reply_markup=' + '{"inline_keyboard": [[{"text": "❌","callback_data":"delete","selective":true}]]}'
                            status = requests.post(url, files=files)
                            found += 1
                            total += 1
                        else:
                            total += 1






                if total == finish :
                    next = page + 1
                    #codal_info|
                    if stat == 'monthly_report' : button = 'codal_info|' + sahm_name + '|' + str(next)

                    #codal_price_increase|
                    if stat == 'price_increase' : button = 'codal_price_increase|' + sahm_name + '|' + str(next)

                    #codal_info|
                    if stat == 'info' : button = 'codal_info|' + sahm_name + '|' + str(next)

                    #codal_total|
                    if stat == 'total' : button = 'codal_total|' + sahm_name + '|' + str(next)

                    #codal_benefit|
                    if stat == 'benefit' : button = 'codal_benefit|' + sahm_name + '|' + str(next)

                    #codal_financestate|
                    if stat == 'financestate' : button = 'codal_financestate|' + sahm_name + '|' + str(next)


                    more_text = '📁 کدال' + '\n' + '🔸 نام سهم : ' + sahm_name + '\n' + '.'
                    inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "↩️ بیشتر","callback_data":button}],[{"text": "❌","callback_data":"delete"}]]})
                    send_inline = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + more_text \
                    + '&reply_markup=' + str(inline_keyboard)
                    requests.get(send_inline)
                    #add stock name into database
                    break

        if found == 0:
            if page == 1 :
                text = '❗️متاسفانه تحلیل سهم مربوطه یافت نشد❗️'
            if page > 1:
                text = '❗️پست های بیشتر یافت نشد❗️'
            url =  self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
            requests.get(url)





    def monthly_report(self,chatid,sahm,page):
        stat = 'monthly_report'
        query_list = ['گزارش فعالیت ماهانه']
        self.crawler(chatid,sahm,query_list,page,stat)

    def price_increase(self,chatid,sahm,page):
        stat = 'price_increase'
        query_list = ['#افزایش_سرمایه']
        self.crawler(chatid,sahm,query_list,page,stat)

    def info(self,chatid,sahm,page):
        stat = 'info'
        query_list = ['#افشای_اطلاعات_با_اهمیت']
        self.crawler(chatid,sahm,query_list,page,stat)

    def total(self,chatid,sahm,page):
        stat = 'total'
        query_list = ['#مجمع']
        self.crawler(chatid,sahm,query_list,page,stat)

    def benefit(self,chatid,sahm,page):
        stat = 'benefit'
        query_list = ['#سود_نقدی']
        self.crawler(chatid,sahm,query_list,page,stat)

    def financestate(self,chatid,sahm,page):
        stat = 'financestate'
        query_list = ['صورت های مالی']
        self.crawler(chatid,sahm,query_list,page,stat)



#sahm = 'غنوش'
#codal().monthly_report(98056633,sahm)
#codal().price_increase(98056633,sahm)
#codal().info(98056633,sahm)
#codal().total(98056633,sahm,page=3)
#codal().benefit(98056633,sahm) --> edit this later
#codal().financestate(98056633,sahm)
