from telethon.sync import TelegramClient
from telethon.sessions import StringSession
from telethon.tl.types import InputMessagesFilterPhotos
import requests
import urllib.parse
import re
import json
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
from persiantools.jdatetime import JalaliDate
import asyncio
import time as timee
from dateutil.relativedelta import relativedelta
import pytz
import config

api_id = 1283044
api_hash = 'c26de812e6d56336b4d06968083ea06b'




class tahlil:
    def __init__(self):
        self.loop = asyncio.new_event_loop()

        #self.token = '1294846811:AAG3NKmiGY4rrrsTs6raJ0m2ARomS1IZXLg'
        self.token = '1093721498:AAF1GWBFnTKfGG1cguLeOZ4r8ppOomNtvFw'
        self.api_url = 'https://api.telegram.org/' + config.token + '/'
        self.session = '1BJWap1sBu3ROBafsOCZ1AMKaE-MHZvSepqR3fWF3l7-VFBXHwxKI8qYSv5xNl4wSZZPYOtm5_4HgX9NLPErAX74N3N0uiv4mlB3fLG5KGFCWPoFTc9HKik3QdRX-Mb88oDHhcGE7GJ3FFcCxKVGZweC9c5AWF6aT5DkAdMKkhQ-fnQrOCObQTTrRO9mKWqNRXYXnZNmGUe-Y3x81hvZv3HXvLuuLW1tTN-el6AV-LzjwdaFw-_qxg19WiIF3wHkcGf7t_w6L-shPGOH9evUORPdyEVnGDVGdTH9b1WPNTDag35k6BWyrlxEr8Uz2Gta3TgXhMdyD0OtDl7q1xBQiXIBmlAeZJ5M='

    #test function
    def test(self):
        with TelegramClient(StringSession(self.session),api_id,api_hash) as client:
            sahm_name = 'خودرو'
            month = date.today() + relativedelta(months=+2)
            search = client.get_messages(-1001319412645,None,search=f'#{sahm_name}',filter=InputMessagesFilterPhotos,offset_date=month)
            print(search.total)
            co = 0
            msglist = list()
            for message in search:

                #print(dir(message))
                jsonmessage = json.loads(message.to_json())
                #print(jsonmessage)
                #print('messageid : ',jsonmessage['id'])
                #print('channelid :', '-100' + str(jsonmessage['peer_id']['channel_id']))
                messagedate = datetime.strptime(str(message.date).replace('+00:00',''),'%Y-%m-%d %H:%M:%S')
                #print('timestamp :',messagedate.timestamp())
                msglist.append({'timestamp':messagedate.timestamp(),'channelid':'-100' + str(jsonmessage['peer_id']['channel_id']),'messageid':jsonmessage['id']})
            for i in msglist:
                print(i)



    async def crawler(self,chatid,sahm_name,ids,page,stat):
        async with TelegramClient(StringSession(self.session),api_id,api_hash) as client:
            total = 0
            found = 0
            start = (page * 4) - 4
            finish = page * 4
            page = int(page)
            msglist = list()
            for id in ids:

                count = 0
                #if type(id) is not int : id = '-100' + str(client.get_input_entity(id).channel_id)
                
                search = await client.get_messages(int(id),None,search=f'#{sahm_name}',filter=InputMessagesFilterPhotos)

                for message in search:

                    tz = pytz.timezone('Asia/Tehran')
                    fromtime = datetime.now(tz) + relativedelta(months= page * -1) # page = 0 --> page - 1 --> month = -1

                    totime = datetime.now(tz) + relativedelta(months=(page - 1 )*-1)
                    #print(f'{fromtime} < {message.date} < {totime}')
                    fromtimee = timee.mktime(fromtime.timetuple())
                    totimee = timee.mktime(totime.timetuple())
                    msgdt = datetime.strptime(str(message.date).replace('+00:00',''),'%Y-%m-%d %H:%M:%S')
                    msgdt = msgdt.timestamp()

                    if  fromtimee <= msgdt <= totimee:
                        count += 1
                        #found += 1
                        total = 1
                        found = 1
                        dt = datetime.strptime(str(message.date).replace('+00:00',''),'%Y-%m-%d %H:%M:%S')
                        jsonmessage = json.loads(message.to_json())
                        msglist.append({'timestamp':dt.timestamp(),'channelid':'-100' + str(jsonmessage['peer_id']['channel_id']),'messageid':jsonmessage['id']})
                        if count == 1 : break

            else:
                if len(msglist) != 0:
                    total = 0
                    json_obj = list()
                    json_obj.append({'predictions':msglist})
                    json_obj = json_obj[0]
                    sorted_obj = dict(json_obj)
                    sorted_obj['predictions'] = sorted(json_obj['predictions'], key=lambda x : x['timestamp'], reverse=True)
                    for i in sorted_obj['predictions']:
                        text = ''
                        #print(i['timestamp'])
                        dt_object = datetime.fromtimestamp(i['timestamp'])
                        chid = int(i['channelid'])
                        msgid = int(i['messageid'])
                        sendit = await client.get_messages(chid,ids=msgid)
                        #print(sendit.message)
                        time = JalaliDate.to_jalali(dt_object).strftime("%Y/%m/%d")

                        text = re.sub('\@\w*','',sendit.message)
                        text += '\n'+ '🗂 مبنع : ' + sendit.sender.title + '\n' + '🗓 تاریخ ارسال پست : ' + time + '\n\n@DastyarBoursebot'
                        text = urllib.parse.quote(text)
                        data = await client.download_media(sendit,file=bytes)

                        #post image and send it to user
                        files = {'photo': data }
                        urlphoto =  self.api_url + 'sendPhoto?chat_id=' + str(chatid) + '&caption=' + text + \
                        '&reply_markup=' + '{"inline_keyboard": [[{"text": "❌","callback_data":"delete","selective":true}]]}'
                        status = requests.post(urlphoto, files=files)
                        total += 1
                        #break




                        if total > 10:
                            break

                if  total != 0:
                    #
                    next = page + 1
                    if stat == 'fundmental' : callback = 'tahlil_fund|' + sahm_name + '|' + str(next)
                    if stat == 'technical' : callback = 'tahlil_tech|' + sahm_name + '|' + str(next)
                    to = str(page + 1)
                    fromm = str(page)
                    more_text = '📊 تحلیل' + '\n' + '🔸 نام سهم : ' + sahm_name + '\n' + '⏱ نمایش پست های بیشتر از ' + fromm + ' تا ' + to + ' ماه اخیر' + '\n.'
                    inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "↩️ بیشتر","callback_data":callback}],[{"text": "❌","callback_data":"delete"}]]})
                    send_inline = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + more_text \
                    + '&reply_markup=' + str(inline_keyboard)
                    requests.get(send_inline)
                                        #if type(id) is not int : time.sleep(2)
                if len(msglist) == 0:
                    #if found == 0:
                    if page == 1 :
                        text = '❗️متاسفانه تحلیل سهم مربوطه یافت نشد❗️'
                        url =  self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
                        requests.get(url)
                    if 1 < page <= 12:
                        # can put normal user limit here
                        next = page + 1
                        to = str(page + 1)
                        fromm = str(page)
                        if stat == 'fundmental' : callback = 'tahlil_fund|' + sahm_name + '|' + str(next)
                        if stat == 'technical' : callback = 'tahlil_tech|' + sahm_name + '|' + str(next)
                        text = '📊 تحلیل' + '\n' + '🔸 نام سهم : ' + sahm_name + '\n' +'پست های بیشتر در این بازه یافت نشد!' + '\n' + '⏱ نمایش پست های بیشتر از ' + fromm + ' تا ' + to + ' ماه اخیر' + '\n.'
                        inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "↩️ بیشتر","callback_data":callback}],[{"text": "❌","callback_data":"delete"}]]})
                        url =  self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text\
                        + '&reply_markup=' + str(inline_keyboard)
                        requests.get(url)

    def fundmental(self,chatid,sahm,page):
        #fundmental channels here
        stat = 'fundmental'
        id = [-1001085059691,-1001386563297,-1001215781354,-1001007006687,-1001074482831,-1001078721519,-1001421450298,-1001080913390,-1001120046276,-1001233007950,-1001290364056]
        asyncio.set_event_loop(self.loop)
        self.loop.run_until_complete(self.crawler(chatid,sahm,id,page,stat))
        self.loop.close()
    def technical(self,chatid,sahm,page):
        stat = 'technical'
        id = [-1001437904621,-1001101106887,-1001186247113,-1001361306194,-1001485977351,-1001119973473,-1001250263717,-1001037282996,-1001381972218,-1001009527894,-1001100676153,
        -1001235182027,-1001284678990,-1001309600376,-1001082829946,-1001177031369,-1001435213682,-1001151673508,-1001165064387,-1001027909987,-1001309747708,-1001292766613
        ,-1001319412645,-1001402821775,-1001054241605,-1001298258586,-1001298417223,-1001155205486,-1001481038926,-1001464851426,-1001293840526,-1001247075296,-1001096389045,-1001345528333,
        -1001272259150,-1001247093027,-1001128493841,-1001026220034,-1001083379888,-1001144244707,-1001104492445,-1001367661135,-1001361322296,-1001317313157,-1001223465561,
        -1001323215186,-1001101568666,-1001364220985,-1001189658165,-1001386452125,-1001102611898,-1001215682455,-1001250005916]
        asyncio.set_event_loop(self.loop)
        self.loop.run_until_complete(self.crawler(chatid,sahm,id,page,stat))
        self.loop.close()
