from tahlilgar.tahlil import tahlil
from tahlilgar.advance import Piot
from payments.vipmember import is_vip
from src.telegram import static
from threading import Thread

def thandler(chatid,text):
    split = text.split('|')
    text = split[0]
    name = split[1]

    if text == 'tahlil_short':
        if is_vip(chatid):
            try:
                Thread(target=Piot,args = [name,'short',str(chatid)]).start()
            except:
                pass
        else:
            static.only_vip(chatid)

    if text == 'tahlil_mid':
        if is_vip(chatid):
            try:
                Thread(target=Piot,args = [name,'mid',str(chatid)]).start()
            except:
                pass
        else:
            static.only_vip(chatid)

    if text == 'tahlil_long':
        if is_vip(chatid):
            try:
                Thread(target=Piot,args = [name,'long',str(chatid)]).start()
            except:
                pass
        else:
            static.only_vip(chatid)

    if text == 'tahlil_fund':
        page = int(split[2])
        tahlil().fundmental(chatid,name,page)

    if text == 'tahlil_tech':
        page = int(split[2])
        tahlil().technical(chatid,name,page)
