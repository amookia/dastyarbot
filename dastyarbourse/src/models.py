from pymongo import MongoClient,DESCENDING
import time
import os
from datetime import datetime,date,timedelta
from dateutil.relativedelta import relativedelta
import pytz
import config

class db:
    def __init__(self):
        client = MongoClient(config.mongo_uri, 27017)
        db = client['financebot']
        self.members = db['members']
        self.user_status = db['user_status']
        self.payment_id = db['payment_id']
        self.verified = db['verified_payments']
        self.vip = db['vipmembers']
        self.normal_user_limit = db['limits']
        self.payam_nazer = db['payamnazer']
        self.prices = db['prices']
        self.invite_rules = db['invite_rules']
        self.static_texts = db['static']
        self.stocks = db['stocks']
        self.baskets = db['baskets']
        self.indicators = db['indicators']
        self.filters = db['filters']
        self.userfilters = db['user_filters']
        self.userportfo = db['user_portfo']
        self.goldensignals = db['golden_signal']
        self.stockscache = db['stockscache']
        self.goldenlimits = db['goldenlimits']

    def set_stat(self,chat_id,user_status):
        self.user_status.update_one({'chat_id':chat_id},{ "$set": { "stat": user_status }})

    def show_stat(self,chat_id):
        stat = self.user_status.find({'chat_id':chat_id})
        for i in stat:
            return i['stat']
            break

    def user_start(self,chat_id):
        find = self.members.count_documents({'chat_id' : chat_id})
        if find == 0 :
            user = {'chat_id' : int(chat_id),'timestamp' : int(time.time())}
            stat = {'chat_id' : int(chat_id),'stat': 'main'}
            self.members.insert_one(user).inserted_id
            self.user_status.insert_one(stat).inserted_id
            return True


    def isert_payment_id(self,jsonlist,chat_id):
        link = jsonlist['link']
        id = jsonlist['id']
        self.payment_id.insert_one({'id':id,'link':link,'chatid':chat_id,'payed':False})

    def find_chatid(self,id):
        chatid = self.payment_id.find_one({'id':id})['chatid']
        return chatid

    def is_payed(self,id):
        stat = self.payment_id.find({'id':id})
        stat = list(stat)[0]['payed']
        return stat

    def verified_payments(self,jsonlist,id):
        if self.is_payed(id) != True:
            self.payment_id.update_one({'id':id},{ "$set": { "payed": True }})
            self.verified.insert_one(jsonlist)

    def create_vip(self,chatid,exptimestamp):
        find = self.vip.count_documents({'chatid':chatid})
        if find == 0:
            self.vip.insert_one({'chatid':chatid,'expire':exptimestamp})
        if find == 1:
            self.vip.update_one({'chatid':chatid},{ "$set": { "expire": exptimestamp }})

    def vip_exp(self,chatid):
        find = self.vip.count_documents({'chatid':chatid})
        if find == 1 :
            timestamp = self.vip.find_one({'chatid':chatid})['expire']
            return timestamp
        if find == 0:
            return -1

    def normal_user_setlimit(self,chatid,sahm,isincode):
        find = self.normal_user_limit.count_documents({'chatid':chatid})
        if find == 1 :
            ulimit = self.normal_user_limit.find_one({'chatid':chatid})
            if ulimit['sahm2'] is None and ulimit['sahm1'] != sahm:
                self.normal_user_limit.update_one({'chatid':chatid},{ "$set": { "sahm2": sahm,'isin2':isincode}})
        if find == 0 :
            self.normal_user_limit.insert_one({'chatid':chatid,'sahm1':sahm,'isin1':isincode,'sahm2':None,'isin2':None})

    def normal_user_limitcount(self,chatid):
        getstocks = self.normal_user_limit.find_one({'chatid':chatid})
        if getstocks is None:
            return 0
        if getstocks['sahm2'] is None:
            return 1
        if getstocks['sahm2'] is not None:
            return 2

    def normal_user_limitshow(self,chatid):
        getstocks = self.normal_user_limit.find_one({'chatid':chatid})
        stocklist = {'sahm1':getstocks['sahm1'],'isin1':getstocks['isin1'],
        'isin2':getstocks['isin2'],'sahm2':getstocks['sahm2']}
        return stocklist

    def payamnazer(self,chatid,active):
        find = self.payam_nazer.count_documents({'chatid':chatid})
        if find == 1 :
            self.payam_nazer.update_one({'chatid':chatid},{"$set": {'active':active}})
        if find == 0:
            self.payam_nazer.insert_one({'chatid':chatid,'active':active})

    def payamnazer_stat(self,chatid):
        user = self.payam_nazer.find_one({'chatid':chatid})

        if user is None:
            return False

        if user['active'] is True:
            return True

        if user['active'] is False:
            return False

    def all_members(self):
        listofmembers = list()
        findall = self.members.find()
        for i in findall:
            listofmembers.append(str(i['chat_id']))
        return listofmembers

    def all_vips(self):
        listofvip = list()
        findall = self.vip.find()
        for i in findall:
            listofvip.append(str(i['chatid']))
        return listofvip

    def invite_tovip(self,chatid,days):
        #fuck cerciular import
        tz = pytz.timezone('Asia/Tehran')
        unixtime = datetime.now(tz).timestamp()
        exp = db.vip_exp(chatid)
        checkvip = self.vip.count({'chatid':chatid})

        #Check if user not vip or expired
        if unixtime > exp or checkvip == 0:
            btime = datetime.now(tz) + relativedelta(days=+days)
            expireunix = time.mktime(btime.timetuple())

            self.create_vip(chatid,expireunix)

        #Check if user is vip then update expire timestamp
        if exp > unixtime:
            timestamp = db.vip.find_one({'chatid':chatid})['expire']

            dtime = datetime.fromtimestamp(timestamp,tz)
            btime = dtime + relativedelta(days=+days)
            expireunix = time.mktime(btime.timetuple())

            self.create_vip(chatid,expireunix)



    def add_invite(self,referal):
        find_user = self.members.find_one({'referal':referal})
        chatid = find_user['chat_id']
        if find_user is not None:
            if 'invites' in find_user:
                invite = find_user['invites'] + 1
            else:
                invite = 1
            db.members.update_one({'chat_id':chatid},{'$set':{'invites':invite}})
            for data in self.invite_rules.find():
                rules = data
                break
            max_invite = rules['invites']
            day = rules['days']
            if invite % max_invite == 0:
                self.invite_tovip(chatid,day)
            return find_user['chat_id']

    def add_basket(self,chatid,text):
        user = self.baskets.find_one({'chatid':chatid})
        text = text.replace('#','')
        if user is not None:
            stocks = user['stocks']
            if text not in stocks and len(stocks) <= 15 :
                stocks.append(text)
                self.baskets.update_one({'chatid':chatid},{'$set':{'stocks':stocks}})
                return True,''

            elif len(stocks) >= 15:
                return False,'❗️تعداد سهم های مجاز بیشتر از سقف مجاز میباشد.'

            else:
                return False,''
        else:
            self.baskets.insert_one({'chatid':chatid,'stocks':[text],
            'rsi':True,'cci':True,'ema':True,'cross':True,'ichimoku':True,'macd':True,'psar':True})
            return True,''

    def del_basket(self,chatid,text):
        user = self.baskets.find_one({'chatid':chatid})
        if user is not None:
            stocks = user['stocks']
            stocks.remove(text)
            db.baskets.update_one({'chatid':chatid},{'$set':{'stocks':stocks}})


    def fetch_indicators(self):
        data = []
        for x in self.indicators.find():
            data.append(x)
        if len(data) == 0: return data
        idx = max(range(len(data)), key=lambda index: data[index]['date'])
        date = data[idx]['date']
        res = []
        for y in data:
            if y['date'] == date:
                res.append(y)
        return res

    def user_can_pay(self,chatid):
        tz = pytz.timezone('Asia/Tehran')
        datas = db.payment_id.find({'chatid':chatid,'payed':True}).sort([("_id", DESCENDING)])
        try:
            latest = datas[0]
            id = latest['_id']
            lastday = datetime.now(tz) - timedelta(hours = 24)
            if id.generation_time.astimezone(tz) > lastday:
                return False
            else:
                return True
        except:
            return True

    #check if user has been limited or not
    #return True if tag used else return False
    def user_golden_limited(self,chatid,tag):
        # tags -> bs , rsm , br , rfm
        limits = db.goldenlimits.find_one({'chatid':chatid})
        if limits is None:
            self.goldenlimits.insert_one({'chatid':chatid,'golden':[tag]})
            return False,0
        try:
            golden = limits['golden']
            self.goldenlimits.update_one({'chatid':chatid},{'$addToSet':{'golden':tag}})
            if tag in golden : return True,len(golden)
            return False,len(golden)
        except:
            self.goldenlimits.update_one({'chatid':chatid},{'$addToSet':{'golden':tag}})
            return False,0

    def user_golden_limit_count(self,chatid):
        limits = db.goldenlimits.find_one({'chatid':chatid})
        try:
            golden = limits['golden']
            return len(golden)
        except:
            return 0




db = db()
