import requests
from src.models import db
from arzvaseke.price import get_price,get_gold
from payments.vipmember import is_vip
from payments import prices
import urllib
import json
import datetime
from persiantools.jdatetime import JalaliDateTime
from persiantools import digits
import pytz
import urllib.parse
import uuid
from saham.saham import sahamtool
import config
import time
from tahlilgar.score_plot import get_image
from bson.objectid import ObjectId
from unidecode import unidecode


class bot:
    def __init__(self):
        token = config.token
        self.api_url = f'https://api.telegram.org/{token}/'
        self.channel1_id = config.channel1
        self.channel2_id = config.channel2
        self.channel1_url = 'https://t.me/joinchat/f2SsLQIkZBRlNTk0'
        self.channel2_url = 'https://t.me/joinchat/AAAAAElxrx4bjhA3uc5U_A'
        self.contact_chatid = '922580095'
        self.bot_sign = '\n\n\n' + '@DastyarBoursebot'
        self.admins = [98056633,142792055,922580095,91712579,76195941,256185708]
        self.pay_url = 'https://tahlilgarbot.w4y.ir/api/generate/'

    #welcome message
    def welcome(self,chatid):
        find_user = db.members.find_one({'chat_id':chatid}) is None
        if find_user:
            welcome_text = db.static_texts.find_one({'type':'start'})
            welcome_url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + welcome_text['text']
            requests.get(welcome_url)

    def pay_thanks(self,chatid,data):
        track_id = data['track_id']
        amount = data['payment']['amount']
        text = ' باتشکر از خرید شما 🙏' + '\n' + '✅ کد رهگیری : ' + track_id + '\n' + \
        ' 💰 مبلغ : ' + amount + 'ریال' + '\n.'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
        requests.get(url)

    #when user send /start
    def start_command(self,chat_id,custom=None):
        chatid = chat_id
        db.user_start(chatid)
        db.set_stat(chat_id,'main')
        reply_keyboard = {"resize_keyboard": True, "keyboard": [[
        {"text":"🧺 سبد سهام"},{"text":"👀 واچ‌لیست"}]
        ,[{"text":"🧮 سیگنال اندیکاتور"},{"text":"🥇 سیگنال طلایی"}]
        ,[{"text":"🗺 نمای بازار"},{"text":"🗄 فیلترها"}]
        ,[{"text": "📈 قیمت لحظه‌ای"},{"text": "🔸 بازدهی سهم"}]
        ,[{"text": "📁 کدال"},{"text": "📊 تحلیل"}]
        ,[{"text": "🔝 ترین‌های امروز"},{"text": "💰 قیمت ارز و سکه"}]
        ,[{"text": "🎫 اشتراک"},{"text":"🌐 پیام ناظر"}]
        ,[{"text":"ارتباط با ادمین 🗣"},{"text": "📚 راهنما"}]
        ,[{"text": "👨‍🏫 تیم سازنده"}]]}
        reply_keyboard = json.dumps(reply_keyboard)
        keyboard = '&reply_markup=' + reply_keyboard

        if custom is not None:
            text = custom
        else:
            text = '⬇️ انتخاب کنید'
        url = self.api_url + 'sendMessage?chat_id=' + str(chat_id) + '&text=' + text + keyboard
        requests.get(url)

    def gheymat_button(self,chatid,stat):
        #bazdehi button
        if stat == 'bazdehisahm' : text = '<b>' + '🔸برای دریافت بازدهی سهم مربوطه نام سهم را ارسال کنید' + '</b>' +\
        '\n\n' + '<b>' + 'برای مثال :' + '</b>' + '\n' + \
        'غنوش' + self.bot_sign
        #gheymatlahzei button
        if stat == 'gheymatlahzei' : text = '<b>' + '🔸برای دریافت اطلاعات سهم مربوطه نام سهم را ارسال کنید' + '</b>' +\
        '\n\n' + '<b>' + 'برای مثال :' + '</b>' +  '\n' + \
        'غپاک' + self.bot_sign
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + \
        '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}' + '&parse_mode=HTML'
        requests.get(url)

    def seke_button(self,chatid):
        cprice = get_price()
        gprice = get_gold()
        dollar = int(cprice[0]['dollar_price'][:-1])
        euro = int(cprice[0]['euro_price'][:-1])
        pound = int(cprice[0]['pound_price'][:-1])
        seket = int(gprice[0]['sekeb'].replace(',','')[:-1])
        sekem = int(gprice[0]['sekee'].replace(',','')[:-1])
        sekenim = int(gprice[0]['nim'].replace(',','')[:-1])
        sekerob = int(gprice[0]['rob'].replace(',','')[:-1])
        sekegr = int(gprice[0]['gerami'].replace(',','')[:-1])
        text = '💰 قیمت ارز و سکه' + '\n\n' + '💵 دلار : ' + f'{dollar:,}'  + '\n\n' +\
        '💶 یورو : ' + f'{euro:,}' + '\n\n' + '💷 پوند : ' + f'{pound:,}' + '\n\n\n' +\
        '🔸 سکه بهار آزادی : ' + f'{seket:,}' + '\n\n' + '🔸سکه امامی : ' + f'{sekem:,}' + '\n\n' +\
        '🔸نیم سکه : ' + f'{sekenim:,}' +'\n\n' + '🔸ربع سکه : ' + f'{sekerob:,}' + '\n\n' +\
        '🔸سکه گرمی : ' + f'{sekegr:,}' + '\n\n'+ 'قیمت‌ها به تومان میباشد' +self.bot_sign

        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
        requests.get(url)


    def tahlil_button(self,chatid):
        text = '📍لطفا نام سهم خود را وارد نمایید.'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text +\
        '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}' + '&parse_mode=HTML'
        requests.get(url)


    def delete(self,chatid,messageid):
        url = self.api_url + 'deleteMessage?message_id=' + str(messageid) +'&chat_id=' + str(chatid)
        requests.get(url)


    def is_member(self,chatid):
        url1 = self.api_url + 'getChatMember?chat_id=' + '-1001232187166' +'&user_id=' + str(chatid)
        url2 = self.api_url + 'getChatMember?chat_id=' + '-1001462925096' +'&user_id=' + str(chatid)
        req1 = requests.get(url1)
        req2 = requests.get(url2)
        if config.test_mode == True: return True
        try:
            status1 = req1.json()['result']['status']
            status2 = req2.json()['result']['status']
            channel1 = ''
            if status1 == 'member' or status1 == 'administrator' or status1 == 'creator' :
                channel1 = 'OK'

            if channel1 == 'OK' and status2 == 'member' or status2 == 'administrator' or status2 == 'creator' :
                return True

            else:
                return False

        except:
            return False

    #To join channel
    def join_channel(self,chatid):
        inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "↩️ کانال ربات دستیاربورس","url":self.channel1_url}],[{"text": "↩️ آموزش بازار سرمایه","url":self.channel2_url}],
        [{"text": "✅ عضو شدم","callback_data":"joined"}]]})
        text = 'برای استفاده از ربات دستیار بورس، ابتدا عضو کانال های زیر شده تا از خدمات ما بهره مند شوید 👇'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + \
        '&reply_markup=' + str(inline_keyboard)
        requests.get(url)

    def contact_admin_text(self,chatid):
        text = 'لطفا پیام خود را ارسال کنید'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text +\
        '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}'
        requests.get(url)

    def contact_admin(self,chatid,text):
        #inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "🚨 پیام اضطراری","callback_data":"emergency|"+str(chatid)}]]})
        stext =  text + '\n\n👤\n' + '/sendtomember:' + str(chatid) + ':send'
        url_contact_pejman = self.api_url + 'sendMessage?chat_id=' + '142792055' + '&text=' + urllib.parse.quote(stext)
        #url_contact_me = self.api_url + 'sendMessage?chat_id=' + self.contact_chatid + stext
        url_message = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + 'پیام شما به ادمین ارسال شد' + '\n' + 'باتشکر'
        print(requests.get(url_contact_pejman).json())
        #requests.get(url_contact_me)
        requests.get(url_message)

    def create_referal(self,chatid,messageid):
        for data in db.invite_rules.find():
            rules = data
            break
        users = rules['invites']
        days = rules['days']
        text = f'📨 شما میتوانید با دعوت کردن {users} نفر به ربات {days} روز اشتراک رایگان دریافت کنید.' + '\n' +\
        'برای دریافت اشتراک رایگان لینک زیر را برای دوستان خود ارسال کنید.👇' + '\n'
        link = 't.me/DastyarBoursebot?start=ref-'
        find = db.members.find_one({'chat_id':chatid})
        if 'referal' in find:
            link += find['referal']
        else:
            uniqueid = uuid.uuid4().hex[:10].upper()
            db.members.update_one({'chat_id':chatid},{'$set':{'referal':uniqueid}})
            link += str(uniqueid)
        text += link
        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text
        requests.get(url)

    def check_referal(self,chatid,code):
        find_user = db.members.find_one({'chat_id':chatid}) is None
        if find_user:
            db.user_start(chatid)
            fromid = db.add_invite(code)

            if fromid is not None:
                text = '✅ یک نفر با موفقیت از طریق لینک شما به ربات اضافه شد.'
                url = self.api_url + 'sendMessage?chat_id=' + str(fromid) + '&text=' + text
                requests.get(url)
                if self.is_member(chatid) != True:
                    welcome_text = db.static_texts.find_one({'type':'start'})
                    welcome_url = self.api_url + 'sendMessage?chat_id=' + str(chat_id) + '&text=' + welcome_text['text']
                    requests.get(welcome_url)
                    self.join_channel(chatid)
                else:
                    self.start_command(chatid)

    def stock_basket_button(self,chatid,messageid=None):
        #Check if user is vip
        #callbacks
        #addbasket --> add into db
        #deletebasket --> delete from db
        #settingbasket --> settings
        if is_vip(chatid):
            text = '👋 سلام !\n 👀 به بخش واچ‌لیست خوش امدی \n» تو این بخش سهم ها تو وارد کن و از اخرین اطلاعات مربوط به قیمت و تحلیل های انجام شده توسط ربات با خبر شو !\n\n@DastyarBoursebot' # change it later
            keyboard = {
            "inline_keyboard":[
            [{"text":"➕ افزودن سهم","callback_data":"addbasket"},{"text":"❌ حذف سهم","callback_data":"deletebasket"}],
            [{"text":"🔍مشاهده سهم ها","callback_data":"showbasket"}],
            [{"text":"⚙️ تنظیمات","callback_data":"settingbasket"}]]}
            keyboard = json.dumps(keyboard)
            if messageid is not None:
                url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' +\
                str(chatid) + '&text=' + text + '&reply_markup=' + keyboard
            else:
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text +\
                '&reply_markup=' + keyboard
            requests.get(url)
        else:
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید 👇'
            requests.get(url)
            static.normal_member(chatid)

    def stock_basket_add(self,chatid):
        status = 'addbasket'
        db.set_stat(chatid, status)
        #bot.start_command(chatid)
        text = '📍لطفا نام سهم مورد نظر خود را وارد نمایید'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + \
        '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}'
        requests.get(url)

    def stock_basket_update(self,chatid,detail):
        keyboard = {
        "inline_keyboard":[
        [{"text":"➕ افزودن سهم","callback_data":"addbasket"},{"text":"❌ حذف سهم","callback_data":"deletebasket"}],
        [{"text":"🔍مشاهده سهم ها","callback_data":"showbasket"}],
        [{"text":"⚙️ تنظیمات","callback_data":"settingbasket"}]]}
        keyboard = json.dumps(keyboard)
        if len(detail) != 0:
            name = detail[0]['name']
            addtodb,err = db.add_basket(chatid, name)
            if addtodb:
                text = f'✅ سهم {name} با موفقیت اضافه شد' + '\n' + \
                '👇برای اضافه کردن سهم های بیشتر نام سهم مورد نظر خود را وارد کنید'

            elif len(err) != 0:
                text = err

            else:
                text = '❗️سهم مورد نظر تکراری میباشد'
        else:
            text = '❗️سهم مورد نظر یافت نشد'

        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + keyboard
        requests.get(url)


    def stock_basket_show(self,chatid,stat):
        stocks = db.baskets.find_one({'chatid':chatid})
        if stat == 'show' : callback = 'sahmshowi';resp = '👇برای نمایش اطلاعات سهم مربوطه روی آن کلیک کنید'
        if stat == 'delete' : callback = 'delbasket';resp = '👇برای حذف سهم مربوطه از سبد روی آن کلیک کنید'
        try:
            stocks = stocks['stocks']
            if len(stocks) == 0:
                raise 'ERROR'
            keyboard = list()
            for stock in stocks:
                if stat == 'show':
                    code = sahamtool.search(stock)[0]['id']
                else:
                    code = stock
                keyboard.append([{"text":"📈 "+ stock,"callback_data":f"{callback}|{code}"}])
            keyboard.append([{"text":"↩️ بازگشت","callback_data":"returnbasket"}])
            inline_keyboard = json.dumps({'inline_keyboard':keyboard})
            text = '&text=' + resp + '&reply_markup=' + inline_keyboard

        except:
            inline_keyboard = json.dumps({'inline_keyboard':[[{"text":"➕ افزودن سهم","callback_data":"addbasket"}],
            [{"text":"↩️ بازگشت","callback_data":"returnbasket"}]]})
            text = '&text=' + '❗️متاسفانه سهمی یافت نشد' + '&reply_markup=' + inline_keyboard

        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + text
        requests.get(url)


    def stock_basket_delete(self,chatid,messageid,stock):
        db.del_basket(chatid, stock)
        keyboard = {
        "inline_keyboard":[
        [{"text":"➕ افزودن سهم","callback_data":"addbasket"},{"text":"❌ حذف سهم","callback_data":"deletebasket"}],
        [{"text":"🔍مشاهده سهم ها","callback_data":"showbasket"}],
        [{"text":"⚙️ تنظیمات","callback_data":"settingbasket"}]]}
        keyboard = json.dumps(keyboard)
        text = f'❗️سهم {stock} از سبد حذف شد'
        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' +\
        str(chatid) + '&text=' + text + '&reply_markup=' + keyboard
        requests.get(url)

    def stock_basket_setting(self,chatid,messageid):
        text = '❌اندیکاتور غیرفعال\n✅ اندریکاتور فعال' + '\n.'
        user = db.baskets.find_one({'chatid':chatid})
        if user is not None:
            rsi = user['rsi']
            cci = user['cci']
            ema = user['ema']
            ichimoku = user['ichimoku']
            macd = user['macd']
            psar = user['psar']
            bollinger = user['bollinger']     # [TODO] insert this in mongo
            stochastic = user['stochastic']   # [TODO] insert this in mongo
        else:
            rsi = True
            cci = True
            ema = True
            ichimoku = True
            macd = True
            psar = True
            stochastic = True
            bollinger = True
            db.baskets.insert_one({'chatid':chatid,'stocks':[],
            'rsi':True,'cci':True,'ema':True,'ichimoku':True,'macd':True,'psar':True,'stochastic':True,'bollinger':True})

        rsi_text = '✅ RSI' if rsi else '❌ RSI'
        cci_text = '✅ CCI' if cci else '❌ CCI'
        ema_text = '✅ EMA' if ema else '❌ EMA'
        ichimoku_text = '✅ ICHIMOKU' if ichimoku else '❌ ICHIMOKU'
        macd_text = '✅ MACD' if macd else '❌ MACD'
        psar_text = '✅ PSAR' if psar else '❌ PSAR'
        stc_text = '✅ STOCHASTIC' if stochastic else '❌ STOCHASTIC'
        blng_text = '✅ BOLLINGER' if bollinger else '❌ BOLLINGER'
        keyboard = {
        "inline_keyboard":[
        [{"text":rsi_text,"callback_data":f"indc|rsi|{str(rsi)}"},{"text":cci_text,"callback_data":f"indc|cci|{str(cci)}"}],
        [{"text":ema_text,"callback_data":f"indc|ema|{str(ema)}"},{"text":psar_text,"callback_data":f"indc|psar|{str(psar)}"}],
        [{"text":ichimoku_text,"callback_data":f"indc|ichimoku|{str(ichimoku)}"},{"text":macd_text,"callback_data":f"indc|macd|{str(macd)}"}],
        [{"text":blng_text,"callback_data":f"indc|bollinger|{str(bollinger)}"},{"text":stc_text,"callback_data":f"indc|stochastic|{str(stochastic)}"}],
        [{"text":"↩️ بازگشت","callback_data":"returnbasket"}]]}

        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' +\
        str(chatid) + '&text=' + text + '&reply_markup=' + json.dumps(keyboard)
        requests.get(url)

    def stock_basket_setupdate(self,chatid,messageid,text,stat):
        db.baskets.update_one({'chatid':chatid},{'$set':{text:stat}})
        self.stock_basket_setting(chatid,messageid)


    def indicator_search(self,chatid,stock=None):
        if is_vip(chatid):
            text = 'برای دریافت اطلاعات بیشتر اندیکاتور مورد نظر خود را انتخاب کنید👇'
            callback = 'showic|'
            keyboard = {
            "inline_keyboard":[
            [{"text":"RSI","callback_data":callback + 'rsi'},{"text":"CCI","callback_data":callback + 'cci'}],
            [{"text":"EMA","callback_data":callback + 'ema'},{"text":'PSAR',"callback_data":callback + 'psar'}],
            [{"text":"ICHIMOKU","callback_data":callback + 'ichimoku'},{"text":"MACD","callback_data":callback + 'macd'}],
            [{"text":"BOLLINGER","callback_data":callback + 'bb'},{"text":"STOCHASTIC","callback_data":callback + 'stoch'}]]}
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + json.dumps(keyboard)
            requests.get(url)
        else:
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید 👇'
            requests.get(url)
            static.normal_member(chatid)

    def indicator_button(self,chatid,messageid,indicator):
        if messageid :
            text = '📍اندیکاتور : ' + indicator.upper() + '\n' + '👇انتخاب کنید\n.'
            callback = 'findic|'
            keyboard = {
            "inline_keyboard":[
            [{"text":"🏷 خرید","callback_data":callback + indicator + "|buy"},
            {"text":"🏷 فروش","callback_data":callback + indicator + "|sell"}]]}
            url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' +\
            str(chatid) + '&text=' + text + '&reply_markup=' + json.dumps(keyboard)
            requests.get(url)


    def indicator_send(self,chatid,messageid,indicator,status):
        if is_vip(chatid):
            if status == 'buy'  : regex = '📈 سیگنال خرید'
            if status == 'sell' : regex = '📉 سیگنال فروش '
            count = db.indicators.count({'indicator':indicator,'message':{'$regex':regex}})
            if count != 0:
                bot.delete(chatid,messageid)
                data = []
                for x in db.indicators.find({'indicator':indicator,'message':{'$regex':regex}}):
                    data.append(x)
                idx = max(range(len(data)), key=lambda index: (data[index]['date']).date())
                date = data[idx]['date']
                res = []
                for y in data:
                    if y['date'].date() == date.date():
                        text = y['message'].replace('\\n','\n')
                        text = urllib.parse.quote(text)
                        files = {'photo':y['photo']}
                        url = self.api_url + 'sendPhoto?chat_id=' + str(chatid) + '&caption=' + text
                        requests.post(url,files=files)

            else:
                text = '❗️متاسفانه اندیکاتور مورد نظر شما پیدا نشد'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
                requests.get(url)
        else:
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید 👇'
            requests.get(url)
            static.normal_member(chatid)

    def filters_welcome(self,chatid):
        if is_vip(chatid):
            keyboard = {
            "inline_keyboard":[
            [{"text":"🤖 فیلترهای ربات","callback_data": "botfilters"},{"text":"👨‍🔧 فیلترهای من","callback_data":"myfilters"}],
            [{"text":"🗑 حذف فیلتر","callback_data":"removefilters"},{"text":"➕ افزودن فیلتر","callback_data":"addfilters"}]]}
            url = self.api_url + 'sendMessage?chat_id=' +\
            str(chatid) + '&text=' + '👇انتخاب کنید' + '&reply_markup=' + json.dumps(keyboard)
            requests.get(url)
        else:
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید 👇'
            requests.get(url)
            static.normal_member(chatid)

    def filters_show(self,chatid,messageid=None,obj_id=None,callback=False):
        if is_vip(chatid):
            if messageid and callback == False:
                filter = db.filters.find_one({"_id": ObjectId(obj_id)})
                filter_text = filter['filter']
                filter_name = filter['name']
                #edit message here
                text = '⏳درحال دریافت اطلاعات'
                url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text
                requests.get(url)
                datas = sahamtool.getfilter(filter_text)
                if datas:
                    text = '🔹 نام فیلتر : ' + filter_name + '\n\n'
                    count = 0
                    for data in datas:
                        if count == 30 : break
                        name = data['name']
                        full_name = data['full_name']
                        final_price = data['close_price']
                        text += f'🔸{name} ({full_name})\n' + f'🔹 آخرین قیمت :  {final_price}\n\n'
                        count += 1
                    text += self.bot_sign.replace('\n\n','')
                else:
                    text = '❗️اطلاعات مربوط به این فیلتر یافت نشد'

                url = self.api_url + 'editMessageText?message_id=' + str(messageid) + '&chat_id=' + str(chatid) + '&text=' + text
            else:
                url = self.api_url + 'editMessageText?message_id=' + str(messageid) + '&chat_id=' + str(chatid) + '&text='
                cat_list = list()
                keyboard = list()
                callback = 'cafilter|'
                categories = db.filters.find({},{'_id':0,'category':1})
                for cat in categories:
                    category = cat['category']
                    if category not in cat_list:
                        cat_list.append(category)
                        keyboard.append([{'text': category + ' 📁','callback_data': callback + category}])
                if len(keyboard) != 0:
                    text = '👇یکی از دسته بندی های زیر را انتخاب کنید'
                    keyboard = {"inline_keyboard":keyboard}
                    url += text + '&reply_markup=' + json.dumps(keyboard)
                else:
                    text = '❗️متاسفانه فیلتری یافت نشد'
                    url += text


            requests.get(url)


        else:
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید 👇'
            requests.get(url)
            static.normal_member(chatid)


    def filters_category_show(self,chatid,messageid,category):
        callback = "sfilter|"
        keyboard = list()
        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text='
        find = db.filters.find({'category':category})
        for filter in find:
            name = filter['name']
            objid = str(filter['_id'])
            keyboard.append([{"text": name + '🔻',"callback_data":callback + objid}])
        if len(keyboard) != 0:
            text = '👇برای نمایش اطلاعات بیشتر یکی از فیلترهای زیر را انتخاب کنید'
            keyboard = {"inline_keyboard":keyboard}
            url += text + '&reply_markup=' + json.dumps(keyboard)
        else:
            text = '❗️متاسفانه فیلتری یافت نشد'
            url += text
        requests.get(url)


    def user_filters_add(self,chatid,messageid=None,name=None,filter=None):
        if name is not None and filter is None:
            text = '👇 شرط فیلتر را وارد کنید'
            print(f'userfilter|{name}')
            db.set_stat(chatid,f'userfilter|{name}')
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + \
            '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}'
            requests.get(url)

        if filter is not None and name is not None:
            checkfilter = sahamtool.checkfilter(filter)
            if checkfilter:
                db.userfilters.insert_one({'chatid':chatid,'name':name,'filter':filter})
                text = '✅ فیلتر با موفقیت اضافه شد'
            else:
                text = '❗️شرط فیلتر اشتباه میباشد'
            bot.start_command(chatid,custom=text)

        if messageid is not None:
            count_filters = db.userfilters.count({'chatid':chatid})
            if count_filters >= 20:
                text = '❗️متاسفانه نمیتوانید بیشتر از 20 فیلتر اضافه کنید'
                url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text
            else:
                bot.delete(chatid,messageid)
                db.set_stat(chatid,'userfilteradd')
                text = '👇نام فیلتر مورد نظر را وارد کنید'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + \
                '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}'
            requests.get(url)

    def user_myfilters(self,chatid,messageid,object_id=None):
        if object_id is not None:
            filter = db.userfilters.find_one({'chatid':chatid,'_id':ObjectId(object_id)})
            filter_name = filter['name']
            filter_real = filter['filter']
            datas = sahamtool.getfilter(filter_real)
            if datas:
                text = '🔹 نام فیلتر : ' + filter_name + '\n\n'
                count = 0
                for data in datas:
                    if count == 30 : break
                    name = data['name']
                    full_name = data['full_name']
                    final_price = data['close_price']
                    text += f'🔸{name} ({full_name})\n' + f'🔹 آخرین قیمت :  {final_price}\n\n'
                    count += 1
                text += self.bot_sign.replace('\n\n','')
            else:
                text = '❗️اطلاعات مربوط به این فیلتر یافت نشد'

            url = self.api_url + 'editMessageText?message_id=' + str(messageid) + '&chat_id=' + str(chatid) + '&text=' + text
            requests.get(url)

        else:
            keyboard = list()
            callback = 'smyfilter|'
            url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text='
            filters = db.userfilters.find({'chatid':chatid})
            for filter in filters:
                name = filter['name']
                id = str(filter['_id'])
                keyboard.append([{"text": name + '🔻',"callback_data": callback + id}])

            if len(keyboard) != 0:
                text = '👇برای نمایش اطلاعات بیشتر یکی از فیلترهای زیر را انتخاب کنید'
                keyboard = {"inline_keyboard":keyboard}
                url += text + '&reply_markup=' + json.dumps(keyboard)
            else:
                keyboard = {"inline_keyboard":[[{"text":"➕ افزودن فیلتر","callback_data":"addfilters"}]]}
                text = '❗️متاسفانه فیلتری یافت نشد' + '&reply_markup=' + json.dumps(keyboard)
                url += text
            requests.get(url)

    def user_removefilters(self,chatid,messageid,object_id=None):
        if object_id is not None:
            db.userfilters.remove({'chatid':chatid,'_id':ObjectId(object_id)})
            text = '✅ فیلتر با موفقیت حذف شد'
            url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text
        else:
            keyboard = list()
            callback = 'defilter|'
            filters = db.userfilters.find({'chatid':chatid})
            url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text='
            for filter in filters:
                name = filter['name']
                id = str(filter['_id'])
                keyboard.append([{"text": name + ' 🗑 ',"callback_data": callback + id}])

            if len(keyboard) != 0:
                text = '👇برای حذف فیلتر مورد نظر آن را انتخاب کنید'
                keyboard = {"inline_keyboard":keyboard}
                url += text + '&reply_markup=' + json.dumps(keyboard)
            else:
                text = '❗️متاسفانه فیلتری یافت نشد'
                url += text
        requests.get(url)


    def portfobutton(self,chatid,messageid=None):
        if messageid is not None:
            count = db.userportfo.count({'chatid':chatid,'exit':False})
            if count >= 20:
                text = '⛔️تعداد سهم های شما بیش از حد مجاز میباشد'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
                requests.get(url)
            else:
                bot.delete(chatid,messageid)
                db.set_stat(chatid,'portfoadd')
                text = '👇نام سهم مورد نظر را وارد نمایید'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text  + \
                '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}'
        else:
            text = '🧺 به سبد سهام خوش اومدی ' + '\n' + 'تو این بخش ورود و خروج از سهم هاتو وارد کن و وضعیت معاملاتت رو زیر نظر بگیر !'
            inline_keyboard = json.dumps({"inline_keyboard": [
            [{"text":"🕵️‍♂️ بررسی کلی","callback_data":"portfocheck"},{"text": "👨‍🔧 سهم های من","callback_data":"myport"}],
            [{"text": "➕ افزودن سهم","callback_data":"addportfo"}]]})
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text  + '&reply_markup=' + inline_keyboard
        requests.get(url)

    def portfohandler(self,chatid,text,isin=None,price=None,vol=None):
        if isin is None:
            stock = db.stocks.find_one({'name':text})
            if stock is not None:
                isin = stock['isin']
                name = stock['name']
                db.set_stat(chatid,f'portfoadd|{isin}')
                price = sahamtool.detail(isin)[0]['today_price']
                text = '👇 قیمت خرید خود را وارد کنید' + '\n' + '🔹نام سهم : ' + name + \
                '\n' + '🔹آخرین قیمت : ' + str(price) + ' ریال' + '\n.'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
            else:
                text = '⛔️ سهم یافت نشد'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
            requests.get(url)
        elif vol is None and isin is not None:
            text = unidecode(text)
            try:
                user_price = int(text)
                if user_price < 0 : user_price = user_price * -1
                text = '👇 حجم خرید خود را وارد کنید'
                db.set_stat(chatid,f'portfoadd|{isin}|{user_price}')
            except:
                text = '⛔️ قیمت نامعتبر'

            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
            requests.get(url)

        else:
            text = unidecode(text)
            try:
                vol = int(vol)
                if vol < 0 : vol = vol * -1
                name = db.stocks.find_one({'isin':isin})['name']
                db.userportfo.insert_one({'name':name,'exit':False,
                'chatid':chatid,'isin':isin,'user_price':price,'volume':vol,
                'time':datetime.datetime.now()})
                text = '✅ سهم با موفقیت به سبد اضافه شد'
                bot.start_command(chatid,custom=text)
            except Exception as e:
                text = '⛔️ حجم نامعتبر'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
                requests.get(url)


    def portfolist(self,chatid,messageid,object_id=None):
        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text='
        if object_id is None:
            keyboard = list()
            callback = 'myport|'
            stocks = db.userportfo.find({'chatid':chatid,'exit':False})
            text = '👇لیست سهم های انتخابی شما' + '\n' + 'برای دریافت اطلاعات بیشتر سهم مورد نظر خود را انتخاب کنید'
            for stock in stocks:
                name = stock['name']
                id = str(stock['_id'])
                keyboard.append([{"text": name + ' 🔺 ',"callback_data": callback + id}])

            if len(keyboard) != 0 : keyboard = {"inline_keyboard":keyboard} ; url += text + '&reply_markup=' + json.dumps(keyboard)
            if len(keyboard) == 0 :
                inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "➕ اضافه سهم","callback_data":"addportfo"}]]})
                url += '⚠️سبد شما خالی میباشد' + '&reply_markup=' + inline_keyboard



        else:
            try:
                portfo = db.userportfo.find_one({'_id':ObjectId(object_id),'chatid':chatid,'exit':False})
                isin = portfo['isin']
                stock = sahamtool.detail(isin)[0]
                stock_price = stock['today_price']
                name = portfo['name']
                user_price = portfo['user_price']
                user_vol = portfo['volume']
                stock_final_price = int(stock_price) * user_vol
                stock_final_price = int(stock_final_price - ((1 * stock_final_price) / 100)) # one percent has been applied on price
                user_final_price = int(user_vol) * int(user_price)
                user_final_price = int( user_final_price - ((1 * user_final_price) / 100)) # one percent has been applied on price
                profit = round(((stock_final_price - user_final_price ) / user_final_price ) * 100 , 2)
                url += f'🔹نام سهم : {name}\n' + f'🔹آخرین قیمت : {stock_price} ریال \n' + f'🔹ارزش کنونی : {stock_final_price} ریال\n'\
                + f'🔻قیمت ورود شما : {user_price} ریال\n' \
                + f'🔻 حجم ورود شما :  {user_vol}\n' + f'🔻قیمت تمام شده :  {user_final_price} ریال\n\n'
                if profit > 0 : url += '📈 درصد سود : ' + str(profit) + ' % \n.'
                if profit < 0 : url += '📉 درصد ضرر : ' + str(profit) + ' % \n.'
                inline_keyboard = json.dumps({"inline_keyboard": [
                [{"text":"↩️ خروج سهم","callback_data":"reportfo|" + object_id},{"text": "🔁 آپدیت","callback_data":"myport|" + object_id}]]})

                url += '&reply_markup=' + inline_keyboard
            except:
                url += '⚠️ سهم مورد نظر از لیست حذف شده است'
        requests.get(url)

    def user_portfo_remove(self,chatid,objectid,price=None,messageid=None):
        if price is None:
            bot.delete(chatid,messageid)
            db.set_stat(chatid,f'portforemove|{objectid}')
            text = '👇قیمت خروج خود را وارد کنید' \
            + '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}'
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
            requests.get(url)
        else:
            price = unidecode(price)
            try:
                price = int(price)
                if price < 0 : price = price * -1
                db.userportfo.update_one({'_id':ObjectId(objectid),'chatid':chatid},
                {'$set':{'exit':True,'exit_price':price}})
                text = '✅  سهم مورد نظر با موفقیت حذف شد'
                bot.start_command(chatid,custom=text)
            except:
                text = '⛔️ قیمت نامعتبر'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
                requests.get(url)

    def user_portfo_check(self,chatid,messageid):
        portfo = db.userportfo.find({'chatid':chatid,'exit':False})
        portfo_exit = db.userportfo.find({'chatid':chatid,'exit':True})
        total_profit_count = 0
        total_loss_count = 0
        total_profit_prs = 0
        total_loss_prs = 0
        total_profit = 0
        total_loss = 0
        bating_avg = 0
        win_avg = 0
        loss_avg = 0
        win_loss_ratio = 0
        #calculating stock on exit
        #stock latest price is user exit price
        for stock_exit in portfo_exit:
            isin = stock_exit['isin']
            stock_price = stock_exit['exit_price']
            user_price = stock_exit['user_price']
            user_vol = stock_exit['volume']
            user_final_price = int(user_vol) * int(user_price)
            stock_final_price = int(user_vol) * int(stock_price)
            stock_final_price = int(stock_final_price - ((1 * stock_final_price) / 100)) # one percent has been applied on price
            user_final_price = int( user_final_price - ((1 * user_final_price) / 100)) # one percent has been applied on price
            profit_cash_exit = stock_final_price - user_final_price
            profit_exit = round(( profit_cash_exit / user_final_price ) * 100 , 2)
            if profit_exit > 0 : total_profit_count += 1 ; total_profit_prs += profit_exit ; total_profit += profit_cash_exit
            if profit_exit < 0 : total_loss_count += 1 ; total_loss_prs += profit_exit ; total_loss += profit_cash_exit
            if portfo_exit.count() > 0 : bating_avg = total_profit_count / portfo_exit.count()
            if total_profit_count > 0 : win_avg = total_profit_prs / total_profit_count
            if total_loss_count > 0 : loss_avg = ( total_loss_prs / total_loss_count ) * -1
            if loss_avg != 0 : win_loss_ratio = win_avg / loss_avg ; win_loss_ratio = round(win_loss_ratio , 2)

        for stock in portfo:
            isin = stock['isin']
            stock_detail = sahamtool.detail(isin)[0]
            stock_price = stock_detail['today_price']
            user_vol = stock['volume']
            user_price = stock['user_price']
            user_final_price = int(user_vol) * int(user_price)
            stock_final_price = int(user_vol) * int(stock_price)
            stock_final_price = int(stock_final_price - ((1 * stock_final_price) / 100)) # one percent has been applied on price
            user_final_price = int( user_final_price - ((1 * user_final_price) / 100)) # one percent has been applied on price
            profit_cash = stock_final_price - user_final_price
            profit = round(( profit_cash / user_final_price ) * 100 , 2)
            if profit > 0 : total_profit_count += 1 ; total_profit_prs += profit ; total_profit += profit_cash
            if profit < 0 : total_loss_count += 1 ; total_loss_prs += profit ; total_loss += profit_cash
            if portfo.count() > 0 : bating_avg = total_profit_count / portfo.count()
            if total_profit_count > 0 : win_avg = total_profit_prs / total_profit_count
            if total_loss_count > 0 : loss_avg = ( total_loss_prs / total_loss_count ) * -1
            if loss_avg != 0 : win_loss_ratio = win_avg / loss_avg ; win_loss_ratio = round(win_loss_ratio , 2)

        inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "🔁 آپدیت","callback_data":"portfocheck"}]]})
        text = f'🔹تعداد معاملات سودآور : {total_profit_count}\n' + f'🔹تعداد معاملات زیان آور : {total_loss_count}\n\n' + \
        f'🔸درصد سود کلی : {total_profit_prs} % \n' + f'🔸 درصد زیان کلی : {total_loss_prs} %\n\n' + \
        f'📈 مقدار سود : {total_profit} ریال \n' + f'📉 مقدار زیان : {total_loss} ریال\n\n' \
        f'🔻bating avg : {bating_avg}\n' + f'🔻average win : {win_avg}\n' + f'🔻average loss : {int(loss_avg)}\n' \
        + f'🔻win loss ratio : {win_loss_ratio}' + '\n\n.'
        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) +\
        '&text=' + text + '&reply_markup=' + inline_keyboard
        requests.get(url)
        return text # for admin

    def golden_signals(self,chatid):
        callback = 'golden|'
        keyboard = json.dumps({"inline_keyboard": [
        [{"text": "bollinger stochastic","callback_data":callback + 'bs'}],
        [{"text": "rsi stochastic macd","callback_data" :callback + 'rsm'}],
        [{"text": "bollinger rsi","callback_data" :callback + 'br'}],
        [{"text": "rsi filter","callback_data": callback + 'rfm'}]]})
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text='
        if is_vip(chatid):
            text = '👇 یکی از گزینه های زیر را انتخاب کنید'
            url += text + '&reply_markup=' + keyboard
        else:
            count = db.user_golden_limit_count(int(chatid))
            if count >= 4:
                url +=  '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید'
            else:
                text = 'اشتراک شما فعال نیست❗️\n' + \
                '📍شما میتوانید از هر استراتژی ها 1 بار استفاده کنید'
                url += text + '&reply_markup=' + keyboard

        requests.get(url)

    def golden_signals_resp(self,chatid,messageid,query):
        limit,count = db.user_golden_limited(int(chatid),query)
        bot.delete(chatid,messageid)
        if is_vip(chatid) or not limit:
            signals = db.goldensignals.find({'category':query})[:10]
            for signal in signals:
                name = signal['name']
                message = urllib.parse.quote(signal['message'])
                photo = signal['photo']
                url = self.api_url + f'sendPhoto?chat_id={chatid}&caption={message}'
                files = {'photo':photo}
                requests.post(url,files=files)
            if signals.count() == 0:
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️متاسفانه سیگنالی یافت نشد'
                requests.get(url)
        else:
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید 👇'
            requests.get(url)
            static.normal_member(chatid)





class sahambot(bot):
    bot = bot()

    def saham_send_details(self,chatid,messageid,details,image=False):
        details = details[0]
        text = '🏷 نام سهم : ' + str(details['name']) + '\n\n' + '♦️ قیمت پایانی : ' + str(details['today_price']) + '\n\n' +\
        '♦️ قیمت امروز : ' + str(details['today_real_price']) + '\n\n' +\
        '♦️ قیمت دیروز : ' + str(details['yesterday_price']) + '\n\n' + '📍درصد تغییر : ' + str(details['ananalytic']) + \
        '\n\n' + '🔻 بیشترین آستانه قیمت : ' + str(details['ht']) + '\n\n' +\
        '🔻 کمترین آستانه قیمت : ' + str(details['lt']) + '\n\n' +\
        '📈 بیشترین : ' + str(details['high_price']) + '\n\n' + \
        '📉 کمترین : ' + str(details['low_price']) + '\n\n' + '📊 ارزش معاملات : ' + str(int(details['arzesh'])/1000000000) + ' B' + '\n\n' + \
        '📄حجم معاملات : ' + 'M ' + str(int(details['hajm_moamelat'])/1000000) + bot.bot_sign
        if image:
            if is_vip(chatid):
                data = get_image(details['xname'])
                files = {'photo': data }
                urlphoto =  self.api_url + 'sendPhoto?chat_id=' + str(chatid) + '&caption=' + text + \
                '&reply_markup=' + '{"inline_keyboard": [[{"text": "❌","callback_data":"delete","selective":true}]]}'
                requests.post(urlphoto,files=files)
            else:
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❗️برای دسترسی به این بخش باید اشتراک تهیه کنید 👇'
                requests.get(url)
                static.normal_member(chatid)

        else:
            url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + '{"inline_keyboard": [[{"text": "❌","callback_data":"delete","selective":true}]]}'
            requests.get(url).json()

    def saham_show(self,chatid,details,stat):
        if stat == 'gheymatlahzei': callback_text = 'sahmshow|'
        if stat == 'bazdehisahm' : callback_text = 'bazdehishow|'
        if stat == 'tahlil' : callback_text = 'tahlil|'
        if stat == 'codal' : callback_text = 'codalshow|'
        if len(details) != 0:
            text = ''
            if stat == 'codal' and is_vip(chatid) is not True: text += '❗️کاربر گرامی اشتراک شما رایگان میباشد.' + '\n' + '❗️شما قادر به انتخاب دو سهم در بخش کدال و تحلیل خواهید بود.' + '\n'
            if stat == 'tahlil' and is_vip(chatid) is not True: text += '❗️کاربر گرامی اشتراک شما رایگان میباشد.' + '\n' + '❗️شما قادر به انتخاب دو سهم در بخش کدال و تحلیل خواهید بود.' + '\n'
            list_show = list()
            inline_keyborad = list()
            for detail in details:
                list_show.append([{'text':urllib.parse.quote_plus('🔰' + detail['name']),'callback_data':callback_text + detail['id']}])
            list_show.append([{"text": "❌","callback_data":"delete"}])
            inline_keyborad =  json.dumps({'inline_keyboard':list_show})
            text += '\n' + '📍لطفا یکی از سهم های زیر را انتخاب کنید' + '\n.' +  '&reply_markup=' + str(inline_keyborad)

        elif len(details) == 0 :
            text = '❗️ متاسفانه سهم مورد نظر پیدا نشد ❗️'

        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
        requests.get(url)

    def top_today_inline(self,chatid):
        text = '❗️یکی از دکمه های زیر را انتخاب کنید❗️'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + \
        '{"inline_keyboard": [[{"text": "📈 بیشترین افزایش","callback_data":"highest_today"},{"text": "📉 بیشترین کاهش","callback_data":"lowest_today"}],[{"text": "❌","callback_data":"delete","selective":true}]]}'
        requests.get(url)

    def top_today_result(self,chatid,messageid,flag):
        wait_url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + '⏳در حال دریافت اطلاعات'
        requests.get(wait_url)
        lists = sahamtool.today_top(flag)
        if len(lists) != 0 :
            text = ''
            for detail in lists:
                text += '🏷 نام سهم : ' + str(detail['name']) + '\n' \
                + '♦️ بیشترین قیمت فروش : ' + str(detail['bestbuyprice']) + '\n'\
                + '♦️ بیشترین قیمت خرید : ' + str(detail['bestsellprice']) + '\n'\
                + '📍درصد تغییر : ' + str(detail['percentchange']) + '\n'\
                + '📍تغییر قیمت : ' + str(detail['pricechange']) + '\n\n'
            text += self.bot_sign
        if len(lists) == 0:
            text = '❗️متاسفانه در حال حاضر امکان نمایش این بخش امکانپذیر نمیباشد ❗️'
        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text +\
        '&reply_markup={"inline_keyboard": [[{"text": "❌","callback_data":"delete","selective":true}]]}'
        requests.get(url)

    def bazdehi_sahm_show(self,chatid,messageid,isin_code):
        base_url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text='
        url = base_url + '⏳در حال دریافت اطلاعات'
        requests.get(url)
        try:
            lists = sahamtool.bazdehi(isin_code)
            text = '🏷 نام سهم : ' + lists['name'] + '\n\n' +\
            '📃 درصد شناور آزاد : ' + str(int(round(lists['FloatPercent'] * 100,2))) + '%' +  '\n\n' +\
            '📊 بازده یک ماه : ' + str(int(lists['E30']*100)) + '%' + '\n' + '📊 بازده سه ماه : ' + str(int(lists['E90']*100)) + '%' + '\n'\
            '📊 بازده یک سال : ' + str(int(lists['E360']*100)) + '%'+ '\n\n' +\
             '🗓 سال مالی : ' + lists['FYear'] + '\n\n' + ' میانگین حجم سه ماه : ' + str(int(lists['Valume90AVG'])) + '\n\n' + \
            '🔹 P/E : ' + str(lists['PE']) + '\n' + '🔹 GP/E : ' + str(int(lists['GPE'])) + '\n' + '🔹 EPS : ' + str(lists['EPS']) + bot.bot_sign
        except:
            lists = sahamtool.bazdehi_backup(isin_code)
            text = '🏷 نام سهم : ' + lists['name'] + '\n\n' +\
            '📃 درصد شناور آزاد : ' + str(lists['FloatPercent']) + "%" + '\n\n' + \
            '🔹 P/E : ' + lists['PE'] + '\n\n' + '🔹 EPS : ' + lists['EPS'] + bot.bot_sign


        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text +\
        '&reply_markup={"inline_keyboard": [[{"text": "❌","callback_data":"delete","selective":true}]]}'
        requests.get(url)


    def tahlil_inline(self,chatid,messageid,name):
        # button 1 --> tahlil_fund|esmesahm --> tahlil_fund|غپاک
        # button 2 --> tahlil_tech|esmesahm --> tahlil_fun|غنوش
        # button 3 --> tahlil_short|esmesahm
        # button 4 --> tahlil_mid|esmesahm
        # button 5 --> tahlil_long|esmesahm
        text = 'یکی از تحلیل های زیر را انتخاب کنید'
        inline_keyboard = json.dumps({"inline_keyboard": [
        [{"text": "تغییر درصدی", "callback_data" : "tahlil_prst|" + str(name) }],
         [{"text": "تحلیل تکنیکال","callback_data":"tahlil_tech|" + str(name) + '|1'},
         {"text": "تحلیل بنیادی","callback_data":"tahlil_fund|" + str(name) + '|1'}],
        [{"text":"ماهانه","callback_data":"tahlil_short|"+str(name)},{"text":"فصلی","callback_data":"tahlil_mid|"+str(name)},
        {"text":"سالانه","callback_data":"tahlil_long|"+str(name)}],[{"text": "❌","callback_data":"delete"}]]})

        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' +\
         text + '&reply_markup=' + str(inline_keyboard)
        requests.get(url)

    def codal_inline(self,chatid,messageid,name):
        text = '📁 کدال' + '\n' + '🔸 نام سهم : ' + name + self.bot_sign + '\n.'
        inline_keyboard = json.dumps({"inline_keyboard": [[{"text": "صورت‌های مالی","callback_data":"codal_financestate|" + str(name) + '|1'},
        {"text": "گزارشات ماهیانه","callback_data":"codal_monthly_report|" + str(name) + '|1'}],
        [{"text": "سود نقدی مالی","callback_data":"codal_benefit|" + str(name) + '|1'},{"text": "مجامع","callback_data":"codal_total|" + str(name) + '|1'}],
        [{"text": "افشای اطلاعات","callback_data":"codal_info|" + str(name) + '|1'},{"text": "افزایش سرمایه","callback_data":"codal_price_increase|" + str(name) + '|1'}],
        [{"text": "❌","callback_data":"delete"}]]})

        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' +\
        text + '&reply_markup=' + str(inline_keyboard)
        requests.get(url)

    def sendstockholder(self,chatid,messageid,sahm):
        text = sahamtool.stockHolder(sahm)
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
        requests.get(url)



class static(bot):
    bot = bot()
    def help_button(self, chatid):
        text = '''📚 راهنما

💰 قیمت ارز و سکه
📚 در این بخش میتوانید آخرین قیمت های سکه و ارز را مشاهده نمایید.

🔸 بازدهی سهم
📚 در این بخش میتوانید با وارد کردن نام سهم مربوطه بازدهی آن را مشاهده نمایید.

📊 تحلیل
📚 در این بخش میتوانید با وارد کردن نام سهم تحلیل های مربوطه آن را از مشاهده نمایید.
⚠️ در صورت نداشتن اشتراک شما قادر به مشاهده دو سهم انتخابی در بخش تحلیل و کدال خواهید بود.

📁 کدال
📚 در این بخش میتوانید با وارد کردن نام سهم و انتخاب تگ مورد نظر از پیام های مربوطه را مشاهده نمایید.
⚠️ در صورت نداشتن اشتراک شما قادر به مشاهده دو سهم انتخابی در بخش تحلیل و کدال خواهید بود.

📈 قیمت لحظه‌ای
📚 در این بخش میتوانید با وارد کردن نام سهم آخرین قیمت واطلاعات سهم مربوطه را مشاهده نمایید.

🔝 ترین‌های امروز
📚 در این بخش میتوانید بیشترین کاهش و بیشترین افزایش سهم های امروز رو مشاهده نمایید.
⚠️ ترین های روز فقط تا ساعت 12 شب قابل مشاهده میباشند.

❗️در صورت بروز هرگونه مشکل میتوانید در قسمت (ارتباط با ادمین 🗣)  با ما درمیان بگذارید تا در اسرع وقت به شما پیام دهیم.

❗️انتقادات و پیشنهادات خود را میتوانید در قسمت  (ارتباط با ادمین 🗣)  برای ما ارسال کنید.
'''
        if chatid in self.admins:
            text += '\n\n\n'
            text +=  '''🔻 راهنمای ادمین :

🔻/user 12345678
✅ ایجاد و حذف اشتراک برای کاربر
آیدی کاربر = 12345678


🔻/members
✅ تعداد کل کاربران ربات


🔻/sendtovip  متن پیام
✅ ارسال پیام به کاربران وی آی پی


🔻/sendtousers متن پیام
✅ ارسال پیام به همه کاربران


🔻/setrpices plan1/plan2/plan3
✅ تغییر قیمت های خرید اشتراک
اشتراک 1 ماهه  = plan1
اشتراک 3 ماهه  = plan2
اشتراک 6 ماهه  = plan3
قیمت اشتراک به ریال وارد شود
📍 مثال
/setprices 500000/1100000/2100000


🔻 /setref days/invites
✅ تغییر مقادیر قسمت دعوت از طریق لینک
روزها = days
تعداد دعوت ها = invites
📍 مثال
/setref 3/5
دریافت 3 روز اشتراک رایگان به ازای دعوت کردن 5 نفر به ربات.


🔻/welcome متن
✅ تغییر متن خوش آمد گویی

🔻/setfilter name
✅ ایجاد یک فیلتر جدید
📍مقدار name برابر با اسم فیلتر میباشد.

🔻 /delfilter
✅حذف فیلتر
'''
        text += '\n.'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
        requests.get(url)

    def vip_member(self,chatid):
        unixdate = datetime.datetime.fromtimestamp(db.vip_exp(chatid))
        date = JalaliDateTime.to_jalali(unixdate).strftime('%H:%M %Y/%m/%d')
        datetime_now = datetime.datetime.now()
        countdown = unixdate - datetime_now
        countdown = countdown.days
        text = '✅ اشتراک شما VIP میباشد.' + '\n' + '🗓 تاریخ انقضا : ' + date
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text='
        if db.user_can_pay(chatid):
            text += '\n\n' + '💰 برای تمدید اشتراک یکی از گزینه های زیر را انتخاب کنید 👇' + '\n.'
            plans = prices.get_price()
            plan1 = plans[0]['price']
            plan2 = plans[1]['price']
            plan3 = plans[2]['price']
            plan4 = plans[3]['price']

            inline_keyboard = json.dumps({"inline_keyboard":
            [[{"text": f"💎 اشتراک 1 ماهه ( {int(plan1/10)} تومان )",'url':self.pay_url + f'{plan1}/' + str(chatid)}],
            [{"text": f"💎 اشتراک 3 ماهه ( {int(plan2/10)} تومان )",'url':self.pay_url  + f'{plan2}/' + str(chatid)}],
            [{"text": f"💎 اشتراک 6 ماهه ( {int(plan3/10)} تومان )",'url':self.pay_url  + f'{plan3}/' + str(chatid)}],
            [{"text": f"💎 اشتراک 12 ماهه ( {int(plan4/10)} تومان )",'url':self.pay_url  + f'{plan4}/' + str(chatid)}]]})
            url += text + '&reply_markup=' + str(inline_keyboard)
        else:
            url += text + '\n.'
        requests.get(url)

    def normal_member(self,chatid):
        plans = prices.get_price()
        plan1 = plans[0]['price']
        plan2 = plans[1]['price']
        plan3 = plans[2]['price']
        plan4 = plans[3]['price']

        text = '❗️کاربر گرامی اشتراک شما رایگان میباشد.' + '\n' + '❗️برای خرید میتوانید یکی از اشتراک های زیر را انتخاب کنید.' + '\n' +\
        '❗️لطفا قبل از پرداخت VPN خود را خاموش کنید 🙏' + '\n.'
        inline_keyboard = json.dumps({"inline_keyboard":
        [[{"text": f"💎 اشتراک 1 ماهه ( {int(plan1/10)} تومان )",'url':self.pay_url + f'{plan1}/' + str(chatid)}],
        [{"text": f"💎 اشتراک 3 ماهه ( {int(plan2/10)} تومان )",'url':self.pay_url  + f'{plan2}/' + str(chatid)}],
        [{"text": f"💎 اشتراک 6 ماهه ( {int(plan3/10)} تومان )",'url':self.pay_url  + f'{plan3}/' + str(chatid)}],
        [{"text": f"💎 اشتراک 12 ماهه ( {int(plan4/10)} تومان )",'url':self.pay_url  + f'{plan4}/' + str(chatid)}],
        [{"text": f"✉️ اشتراک رایگان",'callback_data':'create_referal'}]]}) #Create referal for user
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + str(inline_keyboard)
        requests.get(url)

    def limited_user(self,chatid,status):

        if status == 'codal' : callback = 'codalshow|'
        if status == 'tahlil' : callback = 'tahlil|'
        get_limit = db.normal_user_limitshow(chatid)
        text = '❗️کاربر گرامی متاسفانه اشتراک رایگان شما به اتمام رسیده است.' + '\n' + '❗️برای مشاهده سهم های بیشتر در بخش کدال و تحلیل باید اشتراک تهیه کنید.' + '\n\n.'
        inline_keyboard =  json.dumps({"inline_keyboard": [[{"text": get_limit['sahm1'],"callback_data":callback + get_limit['isin1']}],
        [{"text": get_limit['sahm2'],"callback_data":callback + get_limit['isin2']}]]})
        url = self.api_url +  'sendMessage?chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + str(inline_keyboard)
        requests.get(url)
        self.normal_member(chatid)

    def nazer_inline(self,chatid,messageid=None):
        text = ''
        stat = db.payamnazer_stat(chatid)
        if messageid is not None:
            if stat : text += '❗️وضعیت : فعال ✅' + '\n\n'
            if stat is not True : text += '❗️وضعیت : غیرفعال ❌' + '\n\n'

        text += '🔹با فعال کردن این بخش آخرین پیام های ناظر توسط ربات به شما ارسال خواهد شد.' + self.bot_sign

        if stat: callback = 'deactivenazer';callback_text = '❌ غیرفعال'
        if stat is not True : callback = 'avticenazer';callback_text = '✅ فعال سازی'
        inline_keyboard = json.dumps({"inline_keyboard": [[{"text": callback_text,"callback_data":callback}]]})
        url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + str(inline_keyboard)
        if messageid is None:
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' +  str(inline_keyboard)
        requests.get(url)

    def only_vip(self,chatid):
        text = '❗️فقط کاربران وی آی پی به این بخش دسترسی دارند.'
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
        requests.get(url)

    def creators(self,chatid):
        text = '👨‍🏫 تیم سازنده \n\n» این ربات توسط کدوِب تیم به سفارش کانال سیگنال یاب طراحی و پیاده سازی شده است\n\n» انجام انواع پروژه های برنامه نویسی با بروز ترین متد های برنامه نویسی در کوتاه ترین زمان ممکن\n\n📢 ارتباط با ما\n\n🏷 ایدی ادمین : @code_web_team' #change it later
        url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + urllib.parse.quote(text)
        requests.get(url)

class admin(bot):
    def send_to_all(self,chatid,text):
        if chatid in self.admins:
            to_sender = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '✅ پیام به تمام کاربران ارسال شد.'
            requests.get(to_sender)
            members = db.all_members()
            for chat_id in members:
                url = self.api_url + 'sendMessage?chat_id=' + chat_id + '&text=' + text
                requests.get(url)

    def send_to_vip(self,chatid,text):
        if chatid in self.admins:
            to_sender = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '✅ پیام به کاربران vip ارسال شد.'
            requests.get(to_sender)
            vips = db.vip.find({'expire':{'$gte':time.time()}})
            for user in vips:
                chat_id = str(user['chatid'])
                url = self.api_url + 'sendMessage?chat_id=' + chat_id + '&text=' + urllib.parse.quote(text)
                requests.get(url)

    def send_to_member(self,chatid,userchatid,text):
        if chatid in self.admins:
            sender_url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '✅ پیام با موفقیت ارسال شد.'
            url = self.api_url + 'sendMessage?chat_id=' + userchatid + '&text=' + text + '\n\n❗️ارسال شده توسط ادمین.'
            requests.get(url)
            requests.get(sender_url)

    def memberscount(self,chatid):
        if chatid in self.admins:
            vip_count = db.vip.count({'expire':{'$gte':time.time()}})
            count = db.members.count()
            total = 0
            for i in db.verified.find({'card_no':{'$ne':'123456******1234'}}):
                amount = int(i['amount'])
                total += amount
            sender_url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + 'تعداد کاربران : ' + str(count) + \
            '\n' + 'اعضای وی آی پی : ' + str(vip_count) + '\n' + 'کل درامد : ' + str(total) + 'ریال'
            requests.get(sender_url)

    def set_prices(self,chatid,text):
        if chatid in self.admins:
            try:
                data = text.split('/')
                for idx in data:
                    int(idx)

                prices.update_plans(data[0],data[1],data[2],data[3])
                to_sender = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '✅ قیمت ها بروز رسانی شد.'
                requests.get(to_sender)
            except:
                to_sender = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + '❌ بروز خطا ورودی را چک کنید.'
                requests.get(to_sender)

    def forward(self,chatid,messageid):
        members = db.all_members()
        for member in members:
           forward_url = self.api_url + f'copyMessage?chat_id={member}&from_chat_id={str(chatid)}&message_id={messageid}'
           requests.get(forward_url)

    def userinfo(self,uchatid):
        udetailurl = self.api_url + f'getChat?chat_id={uchatid}'
        details = requests.get(udetailurl).json()
        if details['ok'] == True:
            username = 'ندارد'
            if 'username' in details['result']:
                username = '@' + details['result']['username']
            id = details['result']['id']
            first_name = str(details['result']['first_name'])
            vip_user = is_vip(int(uchatid))
            if vip_user == True : vip_text = '💎 اشتراک : VIP'
            if vip_user == False : vip_text = '💎 اشتراک : عادی'
            text = '📍نام : ' + first_name + '\n' + '👤 آیدی : ' + username + '\n'  +\
            '🔐 آیدی عددی : ' + str(id) + '\n' + vip_text +'\n.'
            return text
        else:
            uchatid = f'{uchatid}'
            return uchatid

    def userdetail(self,chatid,targetid):
        if chatid in self.admins:
            try:
                int(targetid)
                detail_url = self.api_url + f'getChat?chat_id={targetid}'
                detail = requests.get(detail_url).json()
                if detail['ok'] == True:
                    inline_keyboard = json.dumps({"inline_keyboard":
                    [[{"text": "💎 اشتراک 1 ماهه","callback_data":f"vipset:{targetid}:plan1"}], #Change user vip to 1 month
                    [{"text": "💎 اشتراک 3 ماهه","callback_data": f"vipset:{targetid}:plan2"}],  #Change user vip to 3 month
                    [{"text": "💎 اشتراک 6 ماهه","callback_data": f"vipset:{targetid}:plan3"}],  #Change user vip to 6 month
                    [{"text": "❗️ حذف اشتراک","callback_data":f"vipset:{targetid}:remove"}],      #Remove user from vip
                    [{"text":"🧺 سبد سهام","callback_data":f"ubasket:{targetid}"}]]})            #user basket details
                    text = self.userinfo(targetid) + '&reply_markup=' + str(inline_keyboard)
                else:
                    text = 'متاسفانه کاربر مورد نظر پیدا نشد'
            except:
                text = 'ورودی نامعتبر لطفا ورودی را دوباره چک کنید'

        sender_url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
        requests.get(sender_url)

    def setreferal(self,chatid,text):
        if chatid in self.admins:
            try:
                days = int(text.split('/')[0])
                invites = int(text.split('/')[1])
                db.invite_rules.update_one({'type':'invite'},{'$set':{'days':days,'invites':invites}})
                utext = '✅ مقادیر با موفقیت ذخیره شد.'
            except:
                utext = '❗️ لطفا مقادیر ورودی را چک کنید.'
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + utext
            requests.get(url)

    def setwelcome(self,chatid,text):
        if chatid in self.admins:
            db.static_texts.update_one({'type':'start'},{'$set':{'text':text}})
            rtext = '✅ متن با موفقیت تغییر یافت.'
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + rtext
            requests.get(url)

    def setfilter(self,chatid,filter=None,name=None,category=None):
        if chatid in self.admins:
            if name and category and filter is None:
                print(f'setfilter:{name}:{category}')
                db.set_stat(chatid,f'setfilter:{name}:{category}')
                rtext = '👇شرط فیلتر را وارد کنید'
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + rtext
                requests.get(url)

            elif filter is not None:
                #categories = db.filter.find()
                checkfilter = sahamtool.checkfilter(filter)
                if checkfilter:
                    db.filters.insert_one({'name':name,'category':category,'filter':filter})
                    text = '✅ فیلتر با موفقیت اضافه شد'
                else:
                    text = '❗️شرط فیلتر اشتباه میباشد'
                bot.start_command(chatid,custom=text)

            else:
                db.set_stat(chatid,f'csetfilter-{name}')
                cat_list = list()
                cat_text = str()
                categories = db.filters.find({},{'_id':0,'category':1})
                for c in categories:
                    if c['category'] not in cat_list:
                        cat_list.append(c['category'])
                        cat_text += '✅ ' + c['category'] + '\n'

                if len(cat_text) == 0 : cat_text = '❗️هیچ دسته بندی وجود ندارد'
                text = cat_text + '\n' + '👇 لطفا دسته بندی خود را انتخاب کنید' + '\n' + \
                'در صورتی که دسته بندی داخل لیست بالا نبود به لیست اضافه خواهد شد' + \
                '&reply_markup={"resize_keyboard": true, "one_time_keyboard": true, "keyboard": [[{"text": "↩️ بازگشت"}]]}'

                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
                requests.get(url)

    def delfilter(self,chatid,messageid=None,obj_id=None):
        if chatid in self.admins:
            if messageid:
                db.filters.remove({"_id": ObjectId(obj_id)})
                text = '✅ فیلتر مورد نظر با موفقیت حذف شد'
                url = self.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(chatid) + '&text=' + text
                requests.get(url)
            else:
                text = '👇برای حذف فیلتر رو آن کلیک کنید'
                filters = db.filters.find()
                keyboard = list()
                for filter in filters:
                    callback = 'dfilter|'
                    name = filter['name']
                    objid = str(filter['_id'])
                    keyboard.append([{"text": name,"callback_data":callback + objid}])

                keyboard = json.dumps({'inline_keyboard':keyboard})
                url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text + '&reply_markup=' + keyboard
                requests.get(url)

    def userbasket(self,chatid,messageid,user):
        if chatid in self.admins:
            user = int(user)
            basket = db.userportfo.find({'chatid':user})
            text = ''
            flag = True
            for stock in basket:
                name = stock['name']
                enter = stock['user_price']
                text += '🔹 نام سهم : ' + name + '\n' + '🔹 قیمت ورود : ' + enter + '\n'
                if 'exit_price' in stock:
                    exit = stock['exit_price']
                    text += '🔹 قیمت خروج : ' + str(exit) + '\n'

            if len(text) == 0 :
                text = '🔻سبد سهام خالی می باشد'
                flag = False
            url = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
            if flag :
                text = bot.user_portfo_check(user,messageid)
                urll = self.api_url + 'sendMessage?chat_id=' + str(chatid) + '&text=' + text
                requests.get(urll)
            requests.get(url)

bot = bot()
sahambot = sahambot()
static = static()
admin = admin()
