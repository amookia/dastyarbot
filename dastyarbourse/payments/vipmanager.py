from payments.vipmember import tovip
from src.models import db
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
import time,pytz
from src.telegram import bot
import requests


def setuserstat(chatid,messageid,status,adminid):
    tz = pytz.timezone('Asia/Tehran')
    if status == 'plan1':
        btime = datetime.now(tz) + relativedelta(months=+1)
        text = '✅ اشتراک 1 ماهه برای' + str(chatid) + ' فعال شد '

    elif status == 'plan2':
        btime = datetime.now(tz) + relativedelta(months=+3)
        text = '✅ اشتراک 3 ماهه برای' + str(chatid) + ' فعال شد '

    elif status == 'plan3':
        btime = datetime.now(tz) + relativedelta(months=+6)
        text = '✅ اشتراک 6 ماهه برای' + str(chatid) + ' فعال شد '

    elif status == 'remove':
        btime = datetime.now(tz) + relativedelta(months=-1)
        text = '❌ اشتراک ' + str(chatid) + ' غیرفعال شد'

    expireunix = time.mktime(btime.timetuple())
    db.create_vip(int(chatid),expireunix)
    url = bot.api_url + 'editMessageText?message_id=' + str(messageid) +'&chat_id=' + str(adminid) + '&text=' + text
    requests.get(url)
