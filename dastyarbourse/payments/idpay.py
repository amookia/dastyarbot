import requests
from src.models import db
from payments.vipmember import tovip
from payments import prices
import random
from src.telegram import bot

class payments:
    def __init__(self):
        self.api_key = '1f9142ba-bd29-47eb-b011-afe896431b6e'
        self.header = {'X-API-KEY':self.api_key,'X-SANDBOX':'0'} # Sandbox is on (test) Turn it off 'X-SANDBOX':'1' --> testmode
        #self.callback = 'https://8cc6dc31285b.ngrok.io/api/payment'
        self.callback = 'https://tahlilgarbot.w4y.ir/api/payment'

    def pay_link(self,amount,chatid):
        get_amounts = prices.get_price()[4]
        if amount in get_amounts:
            rand = random.randint(100,99999)
            url = 'https://api.idpay.ir/v1.1/payment'
            datas = {'order_id':str(rand),'amount':amount,'callback': self.callback}
            jsondata = requests.post(url,json=datas,headers=self.header).json()
            db.isert_payment_id(jsondata,chatid)
            return jsondata['link']
        else:
            return 'https://t.me/' + 'dastyarboursebot'

    def verify(self,id,orderid):
        url = 'https://api.idpay.ir/v1.1/payment/verify'
        datas = {'id':id,'order_id':orderid}
        verifytext = requests.post(url,json=datas,headers=self.header)
        verifyjson = verifytext.json()
        amount = int(verifyjson['amount'])
        if verifytext.status_code == 200 and verifyjson['status'] == 100 and db.is_payed(id) != True:
            chatid = db.find_chatid(id)
            db.verified_payments(verifyjson,id)
            tovip(chatid,amount)
            bot.pay_thanks(chatid,verifyjson)
            return True
        else:
            return False




payments = payments()
