from src.models import db
import requests


def init_price():
    count_all = db.prices.count()
    count_rules = db.invite_rules.count()
    stocks = db.stocks.count()
    if count_all == 0 or count_rules < 1 :
        price_details = [{'plan':1,'price':400000},{'plan':2,'price':1100000},{'plan':3,'price':2100000},{'plan':4,'price':9000000}]
        db.prices.insert_many(price_details)
        invite_rules = {'type':'invite','days':3,'invites':5}
        db.invite_rules.insert_one(invite_rules)
        start_msg = {'type':'start','text':'🙋‍♂️ به ربات خوش آمدید.'}
        db.static_texts.insert_one(start_msg)



def get_price():
    price_details = []
    amounts = []
    get_prices = db.prices.find()
    for price in get_prices:
        if len(price_details) > 4 : break
        price_details.append(price)
        amounts.append(price['price'])
    price_details.append(amounts)
    return price_details

def get_plan(amount):
    c = 0
    for price in get_price():
        if price['price'] == amount: return price['plan']
        c += 1
        if c > 4 : break

def update_plans(plan1_price,plan2_price,plan3_price,plan4_price):
    db.prices.update_one({'plan':1},{'$set':{'price':int(plan1_price)}})
    db.prices.update_one({'plan':2},{'$set':{'price':int(plan2_price)}})
    db.prices.update_one({'plan':3},{'$set':{'price':int(plan3_price)}})
    db.prices.update_one({'plan':4},{'$set':{'price':int(plan4_price)}})
