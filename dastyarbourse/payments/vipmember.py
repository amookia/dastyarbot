from datetime import datetime,date
from dateutil.relativedelta import relativedelta
from src.models import db
from payments import prices
import time
import pytz


def is_vip(chatid):
    tz = pytz.timezone('Asia/Tehran')
    unixtime = datetime.now(tz).timestamp()
    exp = db.vip_exp(chatid)
    #Check it from tehran timezone
    if unixtime > exp:
        #print('Expired')
        return False
    if exp > unixtime:
        #print('VIP member')
        return True

def tovip(chatid,amount):
    #اینجا بیا چک کن اگه یوزر وی آی پی بود
    #به تاریخ انقضاش تعداد روزارو اضافه کن
    #در غیر اینصورت همون روند قبلی رو ادامه بده
    #app.py -> اینجا بیا چک وی آی پی رو بردار که برای همه لینک پرداخت جنریت کنه
    tz = pytz.timezone('Asia/Tehran')
    if is_vip(chatid):
        user = db.vip.find_one({'chatid':chatid})
        expire = user['expire']
        date = datetime.fromtimestamp(expire)
    else:
        date = datetime.now(tz)


    plan = prices.get_plan(amount)
    if plan == 1 :
        #one month --> 1
        btime = date + relativedelta(months=+1)

    if plan == 2:
        #three month --> 3
        btime = date + relativedelta(months=+3)

    if plan == 3:
        #six month --> 6
        btime = date + relativedelta(months=+6)

    if plan == 4:
        #one year --> 12
        btime = date + relativedelta(months=+12)

    expireunix = time.mktime(btime.timetuple())
    db.create_vip(chatid,expireunix)
