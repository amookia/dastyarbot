from distutils.command.config import config
import requests
from src.telegram import bot,sahambot,static,admin
from src.models import db
from payments.vipmember import is_vip
from payments.vipmanager import setuserstat
from saham.saham import sahamtool
from tahlilgar.tahlil import tahlil
from tahlilgar.thandler import thandler
from tahlilgar.chandler import chandler
from threading import Thread

def callback_handler(chat_id,messageid,text,loop):
    #1
    if text[:9] == 'sahmshow|' or text[:9] == 'sahmshowi':
        if text[:9] == 'sahmshowi':
            isin_code = text[10:]
            image = True
        else:
            isin_code = text[9:]
            image = False

        sahm_detail = sahamtool.detail(isin_code)
        sahambot.saham_send_details(chat_id,messageid,sahm_detail,image)
    #2
    if text[:12] == 'bazdehishow|':
        isin_code = text[12:]
        Thread(target=sahambot.bazdehi_sahm_show,args=[chat_id,messageid,isin_code]).start() #start new thread for avoiding bot freez!

    #3
    if text == 'delete':
        bot.delete(chat_id, messageid)

    #4
    if text == 'joined' and bot.is_member(chat_id):
        bot.delete(chat_id,messageid)
        bot.start_command(chat_id)

    #5
    if text == 'highest_today':
        flag = 'highest'
        Thread(target=sahambot.top_today_result,args=[chat_id,messageid,flag]).start()

    #6
    if text == 'lowest_today':
        flag = 'lowest'
        Thread(target=sahambot.top_today_result,args=[chat_id,messageid,flag]).start()

    #7
    if text[:7] == 'tahlil|':
        isin_code = text[7:]
        #convert isin_code to name = name
        name = sahamtool.get_sahm_name(isin_code)
        if is_vip(chat_id) is not True:
            db.normal_user_setlimit(chat_id,name,isin_code)
        sahambot.tahlil_inline(chat_id,messageid,name)
        #------------------------------------------------------
        # button 1 --> tahlil_fund|esmesahm --> tahlil_fund|غپاک
        # button 2 --> tahlil_tech|esmesahm --> tahlil_fun|غنوش

    #8
    if text[:12] == 'tahlil_fund|' or text[:12] == 'tahlil_tech|' or text[:13] == 'tahlil_short|'\
    or text[:11] == 'tahlil_mid|' or text[:12] == 'tahlil_long|':
        bot.delete(chat_id, messageid)
        thandler(chat_id, text)

    #9
    if text[:10] == 'codalshow|':
        isin_code = text[10:]
        name = sahamtool.get_sahm_name(isin_code)
        if is_vip(chat_id) is not True:
            db.normal_user_setlimit(chat_id,name,isin_code)
        sahambot.codal_inline(chat_id,messageid,name)

    #10
    if text[:6] == 'codal_':
        bot.delete(chat_id, messageid)
        chandler(chat_id, text)

    if text == 'avticenazer':
        db.payamnazer(chat_id,True)
        static.nazer_inline(chat_id,messageid)

    if text == 'deactivenazer':
        db.payamnazer(chat_id,False)
        static.nazer_inline(chat_id,messageid)

    if text[:6] == 'vipset':
        splited = text.split(':')
        userid = splited[1]
        status = splited[2]
        setuserstat(userid,messageid,status,chat_id)

    if text == 'create_referal':
        bot.create_referal(chat_id,messageid)

    if text[:12] == 'tahlil_prst|':
        sahm = text[12:]
        bot.delete(chat_id, messageid)
        Thread(target=sahambot.sendstockholder,args=[chat_id,messageid,sahm]).start()

    if text == 'addbasket':
        bot.delete(chat_id, messageid)
        bot.stock_basket_add(chat_id)

    if text == 'showbasket':
        stat = 'show'
        bot.delete(chat_id, messageid)
        bot.stock_basket_show(chat_id,stat)

    if text == 'deletebasket':
        stat = 'delete'
        bot.delete(chat_id, messageid)
        bot.stock_basket_show(chat_id,stat)

    if text[:9] == 'delbasket':
        stock = text[10:]
        bot.stock_basket_delete(chat_id, messageid, stock)

    if text == 'settingbasket':
        bot.stock_basket_setting(chat_id, messageid)

    if text[:4] == 'indc':
        splited = text.split('|')
        stat = splited[2]
        if stat == 'False' :
            stat = True
        else :
            stat = False
        text = splited[1]
        bot.stock_basket_setupdate(chat_id, messageid, text,stat)

    if text == 'returnbasket':
        bot.stock_basket_button(chat_id,messageid)

    if text[:7] == 'findic|':
        splited = text.split('|')
        indicator = splited[1]
        status = splited[2]
        Thread(target=bot.indicator_send,args=[chat_id,messageid,indicator,status]).start()

    if text[:7] == 'showic|':
        splited = text.split('|')
        indicator = splited[1]
        bot.indicator_button(chat_id,messageid,indicator)

    if text[:9] == 'cafilter|':
        category = text[9:]
        print(category)
        bot.filters_category_show(chat_id,messageid,category)

    if text[:8] == 'sfilter|':
        obj_id = text[8:]
        Thread(target=bot.filters_show,args=[chat_id,messageid,obj_id]).start()

    if text[:8] == 'dfilter|':
        obj_id = text[8:]
        admin.delfilter(chat_id,messageid,obj_id)

    if text == 'botfilters':
        if is_vip(chat_id):
            bot.filters_show(chatid=chat_id,messageid=messageid,callback=True)

    if text == 'addfilters':
        if is_vip(chat_id):
            bot.user_filters_add(chat_id,messageid)

    if text == 'myfilters':
        if is_vip(chat_id):
            bot.user_myfilters(chat_id,messageid)

    if text[:10] == 'smyfilter|':
        if is_vip(chat_id):
            object_id = text[10:]
            bot.user_myfilters(chat_id,messageid,object_id=object_id)

    if text == 'removefilters':
        bot.user_removefilters(chatid=chat_id,messageid=messageid)

    if text[:9] == 'defilter|':
        object_id = text[9:]
        bot.user_removefilters(chatid=chat_id,messageid=messageid,object_id=object_id)

    if text[:9] == 'addportfo':
        bot.portfobutton(chat_id,messageid)

    if text[:6] == 'myport':
        if len(text) > 6:
            object_id = text[7:]
            bot.portfolist(chat_id,messageid,object_id)
        else:
            bot.portfolist(chat_id,messageid)

    if text[:9] == 'reportfo|':
        object_id = text[9:]
        bot.user_portfo_remove(chatid=chat_id,messageid=messageid,objectid=object_id)

    if text == 'portfocheck':
        bot.user_portfo_check(chat_id,messageid)

    if text[:7] == 'ubasket':
        user = text[8:]
        admin.userbasket(chat_id,messageid,user)

    if text[:6] == 'golden' :
        print(text)
        query = text[7:]
        bot.golden_signals_resp(chat_id,messageid,query)
