#Handeling User Messages
from src.models import db
from src.telegram import bot,sahambot,static
from saham.saham import sahamtool
from src.telegram import bot,admin
from payments.vipmember import is_vip
from threading import Thread


def text_handler(chat_id,text):
    stat = db.show_stat(chat_id)
    print(stat)
    #1
    if stat == 'tahlil':
        if db.normal_user_limitcount(chat_id) == 2 and is_vip(chat_id) is not True:
            static.limited_user(chat_id,'tahlil')
        if db.normal_user_limitcount(chat_id) < 2 or is_vip(chat_id) is True:
            det = sahamtool.search(str(text))
            sahambot.saham_show(chat_id,det,stat)

    #2
    elif stat == 'bazdehisahm':
        stat == 'bazdehisahm'
        det = sahamtool.search(str(text))
        sahambot.saham_show(chat_id,det,stat)
    #3
    elif stat == 'gheymatlahzei':
        stat = 'gheymatlahzei'
        det = sahamtool.search(str(text))
        sahambot.saham_show(chat_id,det,stat)
    #4
    elif stat == 'contactus':
        bot.contact_admin(chat_id,text)
        bot.start_command(chat_id)

    #5
    elif stat == 'codal':
        if db.normal_user_limitcount(chat_id) == 2 and is_vip(chat_id) is not True:
            static.limited_user(chat_id,'codal')
        if db.normal_user_limitcount(chat_id) < 2 or is_vip(chat_id) is True:
            det = sahamtool.search(str(text))
            sahambot.saham_show(chat_id,det,stat)

    elif stat == 'addbasket':
        det = sahamtool.search(str(text))
        bot.stock_basket_update(chat_id,det)

    elif stat == 'findindicator':
        bot.indicator_search(chat_id,text)

    elif stat[:10] == 'csetfilter':
        name = stat[11:]
        category = text
        admin.setfilter(chat_id,name=name,category=category)

    elif stat[:9] == 'setfilter':
        splited = stat.split(':')
        name = splited[1]
        category = splited[2]
        print('name : ',name,'category : ',category)
        Thread(admin.setfilter(chat_id,filter=text,name=name,category=category)).start()


    if stat == 'userfilteradd':
        if is_vip(chat_id):
            bot.user_filters_add(chatid=chat_id,name=text)

    if stat[:11] == 'userfilter|':
        name = stat[11:]
        Thread(bot.user_filters_add(chat_id,name=name,filter=text)).start()

    if stat[:9] == 'portfoadd':
        if len(stat) > 9:
            splited = stat.split('|')
            if len(splited) > 2:
                isin = splited[1]
                price = splited[2]
                bot.portfohandler(chat_id,text,isin,price=price,vol=text)
            else:
                isin = splited[1]
                bot.portfohandler(chat_id,text,isin)
        else:
            bot.portfohandler(chat_id,text)

    if stat[:13] == 'portforemove|':
        object_id = stat[13:]
        bot.user_portfo_remove(chatid=chat_id,objectid=object_id,price=text)
