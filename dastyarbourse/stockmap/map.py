import requests
import urllib.parse
import io,os
from PIL import Image as Img
from PIL import ImageOps
from PIL import ImageDraw
from PIL import ImageFont
import uuid
from persiantools.jdatetime import JalaliDateTime
import os,glob
import time,datetime
from dateutil.relativedelta import relativedelta
from base64 import decodebytes
from saham.saham import sahamtool
import config

def fileE():
    try:
        filename = glob.glob('./*.png')[0].replace('./','').replace('.\\','')
        filetime = int(filename.replace('.png',''))
        if time.time() > filetime:
            return True
        else:
            return False
    except:
        return True

def remove_png():
    pngs = glob.glob('./*.png')
    for dir in pngs:
        os.remove(dir)


def send_map(chatid):
    jdate = JalaliDateTime.now().strftime('%Y/%m/%d')
    token = config.token
    #Wait for message
    wait_msg = '⏳ لطفا چند لحظه صبر کنید'
    wait_url = f'https://api.telegram.org/{token}/sendMessage?chat_id={str(chatid)}&text={wait_msg}'
    wait_msgid = requests.get(wait_url).json()['result']['message_id']
    ##Delete message url
    delete_url = f'https://api.telegram.org/{token}/deleteMessage?chat_id={str(chatid)}&message_id={wait_msgid}'
    text = '🗺 نقشه بازار' + '\n\n' + '🗓 تاریخ : ' + jdate
    tel_url = f'https://api.telegram.org/{token}/sendPhoto?chat_id={str(chatid)}&caption={text}'
    text = sahamtool.market()
    nbazrurl = f'https://api.telegram.org/{token}/sendMessage?chat_id={str(chatid)}&text={text}'
    requests.get(nbazrurl)
    if fileE():
        data = {'key':'3XCIXwTZ3pVBmbmRYqSiH2fxFNusfYEwksInuJAisOAao1L31P','url':'http://new.tsetmc.com/treemap','height':768,'width':1440}
        try:
            screen = requests.post('http://screeenly.com/api/v1/fullsize',json=data,timeout=50)
            image_url = screen.json()['path']
            if screen.status_code == 200:
                get_image = requests.get(image_url).content
                d = io.BytesIO(get_image)
                btime = datetime.datetime.now() + relativedelta(hours=+1)
                unixtime = int(time.mktime(btime.timetuple()))
                img = Img.open(d)
                border = (20 , 50 , 20 , 20) # left, up, right, bottom
                img = ImageOps.crop(img, border)
                drawing = ImageDraw.Draw(img)
                # Font & font size
                font = ImageFont.truetype('./stockmap/arial.ttf',20)
                drawing.text((5,1000),'@DastyarBoursebot',font=font,fill=10)
                #Image to Bytes
                remove_png() #Remove last png file
                temp = io.BytesIO()
                bytesI = img.save(temp,'png')
                img.save(str(unixtime)+'.png','png')
                requests.get(delete_url)
                files = {'photo':temp.getvalue()}
                requests.post(tel_url,files=files)
        except Exception as e:
            requests.get(delete_url)


    elif fileE() is not True:
        btime = datetime.datetime.now() + relativedelta(hours=+1)
        unixtime = int(time.mktime(btime.timetuple()))
        filename = glob.glob('./*.png')[0].replace('./','').replace('.\\','')
        img = Img.open(filename)
        try:
            update_time = int(filename.replace('.png',''))
            utime = datetime.datetime.fromtimestamp(update_time) - relativedelta(hours=+1)
            utimestamp = utime.timestamp()
            ptime = JalaliDateTime.fromtimestamp(utimestamp).strftime('%H:%M %m/%d')
            tel_url += '\n\n' +  f'🔄 آخرین آپدیت : {ptime}'
        except:
            pass
        temp = io.BytesIO()
        bytesI = img.save(temp,'png')
        requests.get(delete_url)
        files = {'photo':temp.getvalue()}
        requests.post(tel_url,files=files)
