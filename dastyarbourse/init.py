from datetime import datetime,date
from dateutil.relativedelta import relativedelta
from src.models import db
from persiantools.jdatetime import JalaliDateTime
import time
import config
from saham.saham import sahamtool
import requests
from threading import Thread


def send_message(chat_id,text):
    api_url = f'https://api.telegram.org/{config.token}/'
    url = api_url + 'sendMessage?chat_id=' + str(chat_id) + '&text=' + text
    requests.get(url)

def cache_update():
    while True:
        #send_message('98056633','started')
        sahamtool.repeater()
        time.sleep(10*60)

Thread(cache_update()).start()
