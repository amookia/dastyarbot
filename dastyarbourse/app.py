from flask import Flask,request,redirect,render_template
from src.telegram import bot,sahambot,static,admin
from src.models import db
from handlers.text_handler import text_handler
from handlers.callback_handler import callback_handler
from payments.idpay import payments
from payments.vipmember import is_vip
from payments import prices
from stockmap import map
import asyncio
from threading import Thread
from saham.init import filter_update
import config


app = Flask(__name__)


loop = asyncio.get_event_loop()

@app.route('/botishere',methods=['POST'])
def tele():
    try:
        if 'message' in request.json:
            message = request.json['message']
            user_chat_id = request.json['message']['from']['id']
            #User must join in channel

            if 'text' in request.json['message']:
                uchatid = request.json['message']['from']['id']
                utext = request.json['message']['text']
                if utext[:11] == '/start ref-':
                    bot.welcome(uchatid)
                    code = utext[11:]
                    Thread(target=bot.check_referal,args=[uchatid,code]).start()

            if bot.is_member(user_chat_id):
                if 'text' in message:
                    user_text = request.json['message']['text']
                    #/start command
                    if user_text == '/start' or user_text == '↩️ بازگشت' :
                        bot.welcome(user_chat_id)
                        bot.start_command(user_chat_id)

                    elif user_text == '🌐 پیام ناظر':
                        static.nazer_inline(user_chat_id)
                    #bazdehi sahm
                    elif user_text == '🔸 بازدهی سهم':
                        stat = 'bazdehisahm'
                        db.set_stat(user_chat_id,stat)
                        bot.gheymat_button(user_chat_id,stat)
                        #print(stat)

                    #Tahlil
                    elif user_text == '📊 تحلیل':
                        stat = 'tahlil'
                        db.set_stat(user_chat_id,stat)
                        bot.tahlil_button(user_chat_id)

                    #Codal
                    elif user_text == '📁 کدال':
                        stat = 'codal'
                        db.set_stat(user_chat_id,stat)
                        bot.tahlil_button(user_chat_id)

                    #Gheymat lahzei
                    elif user_text == '📈 قیمت لحظه‌ای':
                        stat = 'gheymatlahzei'
                        db.set_stat(user_chat_id,stat)
                        bot.gheymat_button(user_chat_id,stat)

                    elif user_text == '🔝 ترین‌های امروز':
                        sahambot.top_today_inline(user_chat_id)

                    elif user_text == '💰 قیمت ارز و سکه':
                        bot.seke_button(user_chat_id)

                    elif user_text == '🗺 نمای بازار':
                        to_id_map = str(user_chat_id)
                        Thread(target = map.send_map,args=[user_chat_id]).start()

                    #Eshterak
                    elif user_text == '🎫 اشتراک' or user_text == '/start payed':
                        if is_vip(user_chat_id):
                            static.vip_member(user_chat_id)
                        if is_vip(user_chat_id) is not True:
                            static.normal_member(user_chat_id)
                        #print('Eshterak')

                    elif user_text == '📚 راهنما':
                        static.help_button(user_chat_id)
                    #Pishnahad
                    elif user_text == 'ارتباط با ادمین 🗣':
                        stat = 'contactus'
                        db.set_stat(user_chat_id,stat)
                        bot.contact_admin_text(user_chat_id)
                        #print(stat)

                    elif user_text == '👀 واچ‌لیست':
                        bot.stock_basket_button(user_chat_id)

                    elif user_text == '🧮 سیگنال اندیکاتور':
                        bot.indicator_search(user_chat_id)

                    elif user_text == '🗄 فیلترها':
                        bot.filters_welcome(user_chat_id)

                    elif user_text == '🧺 سبد سهام':
                        bot.portfobutton(user_chat_id)

                    elif user_text == '🥇 سیگنال طلایی':
                        bot.golden_signals(user_chat_id)

                    elif user_text == '👨‍🏫 تیم سازنده':
                        static.creators(user_chat_id)

                    #Send message to all members
                    elif user_text[:12] == '/sendtousers':
                        textmsg = user_text[12:]
                        Thread(target = admin.send_to_all, args=(user_chat_id,textmsg)).start()

                    #Send message to only vips members
                    elif user_text[:10] == '/sendtovip':
                        textmsg = user_text[11:]
                        Thread(target=admin.send_to_vip,args=(user_chat_id,textmsg)).start()

                    elif user_text[:13] == '/sendtomember':
                        text = user_text.split(':')[2].replace('send','')
                        tochatid = user_text.split(':')[1]
                        admin.send_to_member(user_chat_id,tochatid,text)

                    elif user_text == '/members':
                        admin.memberscount(user_chat_id)

                    elif user_text[:10] == '/setprices':
                        textmsg = user_text[11:]
                        Thread(target = admin.set_prices, args=(user_chat_id,textmsg)).start()

                    elif user_text[:5] == '/user':
                        uchatid = user_text[6:]
                        admin.userdetail(user_chat_id,uchatid)

                    elif user_text[:7] == '/setref':
                        text = user_text[7:]
                        admin.setreferal(user_chat_id,text)

                    elif user_text[:8] == '/welcome':
                        text = user_text[8:]
                        admin.setwelcome(user_chat_id, text)

                    elif user_text[:10] == '/setfilter':
                        text = user_text[11:]
                        admin.setfilter(user_chat_id,name=text)

                    elif user_text == '/delfilter':
                        admin.delfilter(user_chat_id)

                    else:
                        text_handler(user_chat_id,user_text)
            else:
                bot.welcome(user_chat_id)
                bot.join_channel(user_chat_id)

        if 'callback_query' in request.json:
            callback_data = request.json['callback_query']['data']
            callback_user_chatid = request.json['callback_query']['from']['id']
            callback_messageid  = request.json['callback_query']['message']['message_id']
            if bot.is_member(callback_user_chatid) :
                callback_handler(callback_user_chatid,callback_messageid,callback_data,loop)

            else:
                if callback_data != 'joined':
                    bot.join_channel(callback_user_chatid)

        if 'channel_post' in request.json:
            channel_msgid = request.json['channel_post']['message_id']
            channel_chatid = request.json['channel_post']['chat']['id']
            if -1001332051264 == channel_chatid:
                Thread(target = admin.forward,args = [channel_chatid,channel_msgid]).start()

    except Exception as e:
        print(e)





    return 'OK'

@app.route('/api/payment',methods=['POST'])
def payment():
    paydet = dict(request.form)
    status = paydet['status']
    if status == '10':
        id = paydet['id']
        orderid = paydet['order_id']
        if payments.verify(id, orderid):
            smsg = '...در حال انتقال به ربات'
            return render_template('success.html',message=smsg)
    if status == '6':
        error_text = 'متاسفانه پرداخت شما ناموفق بود.' + '\n' + 'در صورت کسر وجه در 72 ساعت اخیر وجه به حساب شما ارسال خواهد شد.'
        return render_template('failed.html',message=error_text)

    msg = 'پرداخت ناموفق بود لطفا دوباره امتحان کنید'
    return render_template('failed.html',message=msg)

@app.route('/api/generate/<int:price>/<int:chatid>')
def genereate(price,chatid):
    if db.user_can_pay(chatid):
        is_user_vip = is_vip(chatid)
        link = payments.pay_link(price,chatid)
        return redirect(link)
    else:
        msg_fail =  'کاربر گرامی امکان تمدید تا 24 ساعت پس از خرید میسر میباشد'
        return render_template('failed.html',message=msg_fail)








if __name__ == '__main__':
    debug = config.test_mode
    if debug != True:
        filter_update()
        prices.init_price()
        #Thread(target=warn_vips,args=[]).start()
        
    app.run(port=5000,debug=debug,use_reloader=False)
