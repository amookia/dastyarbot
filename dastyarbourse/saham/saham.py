import requests
from urllib3.exceptions import InsecureRequestWarning
import json
from src.models import db
#from datetime import datetime
#import pymongo

class sahamtool:
    #def __init__(self):
        #self.todaytop_url = 'https://gateway.tadbirrlc.com/services/v1/best-of-market-with-first-queue'

    def repeater(self):
        try:
            url = 'http://sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&all'
            #latest = db.stockscache.find_one(sort=[('time', -1)])
            stocks_list = requests.get(url).json()
            for stock in stocks_list:
                isin = stock['namad_code']
                #stock['update_time'] = datetime.now()
                db.stockscache.find_one_and_update({'namad_code':isin},{'$set':stock},upsert=True)
        except:
            pass

    def cache_json(self,id):
        stock = db.stockscache.find_one({'namad_code':id})
        if stock is None:
            name = db.stocks.find_one({'isin':id})['name']
            url = f'https://sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&name={name}'
            sahmlist = requests.get(url).json()
            namad_code = sahmlist['namad_code']
            if len(namad_code) > 0 :
                db.stockscache.find_one_and_update({'namad_code':id},{'$set':sahmlist},upsert=True)
                return sahmlist
            else:
                return sahmlist
        else:
            return stock

    def detail(self,id):
        sahamdetail = list()
        #name = db.stocks.find_one({'isin':id})['name']
        #url = f'https://sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&name={name}'
        sahmlist = self.cache_json(id)
        sahm_name = sahmlist['name']
        name = sahmlist['name'] + ' (' + sahmlist['full_name'] + ')'
        ys_price = str(sahmlist['yesterday_price'])                      #yesterday_price
        today_real_price = str(sahmlist['close_price'])                  #today real price
        today_price = str(sahmlist['final_price'])                       #final price
        high_price = str(sahmlist['highest_price'])                      #highest_price
        low_price = str(sahmlist['lowest_price'])                        #lowest_price
        arzesh = str(sahmlist['trade_value'])                            #arzesh_moamelat
        hajm_mabna = str(sahmlist['basis_volume'])                       #hajm_mabna
        hajm_moamelat = str(sahmlist['trade_volume'])                    #hajm
        ananalytic = str(sahmlist['close_price_change_percent'])         #ananalytic
        ananalytic_price = str(sahmlist['close_price_change'])           #ananalytic_price
        lt = str(sahmlist['daily_price_low'])                            #lowest threshold price
        ht = str(sahmlist['daily_price_high'])                           #highest threshold price

        #Kiri_tarin_api_gharn = sourcearena
        try:
            #if those values 0 then use api for checking prices
            if hajm_moamelat == 0 and arzesh == 0:
                req = self.cache_json(id)
                hajm_moamelat = int(req['trade_volume'])
                arzesh = int(req['trade_value'])
        except:
            pass

        #append details into the list
        sahamdetail.append({'est':sahm_name,'name':name,'today_real_price':today_real_price,'yesterday_price': ys_price,'today_price':today_price
        ,'high_price':high_price,'low_price':low_price,'arzesh':arzesh,'hajm_moamelat':hajm_moamelat
        ,'hajm_mabna':hajm_mabna,'ananalytic':ananalytic,'ananalytic_price':ananalytic_price,'xname':sahm_name
        ,'ht':ht,'lt':lt})
        return sahamdetail


    def search(self,name):
        #using api for geting stock isin code
        ret_list = list()
        result = db.stocks.find_one({'name':name})
        if result is not None:
            ret_list.append({'id':result['isin'],'name':result['name']})
            return ret_list
        else:
            try:
                url = f'https://sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&name={name}'
                get = requests.get(url,timeout=3)
                isin = get.json()['namad_code']
                if len(isin) != 0:
                    db.stocks.insert_one({'name':name,'isin':isin})
                    ret_list.append({'id':isin,'name':name})
                return ret_list
            except:
                return ret_list



    def today_top(self,flag):
        top_list = list()
        url = 'http://sourcearena.ir/api/customapi.php?token=ac3ed71d929300f45b29445f5a844632&cs='

        #HIGHEST
        if flag == 'highest':
            highest = requests.get(url + 'up').json()
            highest = list(filter(lambda x:x["market"]=="بورس",highest))[:10]
            for det in highest:
                name = det['name'] + ' (' + det['name'] + ')'
                top_list.append({
                'name':name,'bestbuyprice':det['3_sell_price'],
                'bestsellprice':det['3_sell_volume'],
                'pricechange':det['close_price_change'],
                'percentchange':det['close_price_change_percent']})

        #LOWEST
        elif flag == 'lowest':
            lowest = requests.get(url + 'down').json()
            lowest = list(filter(lambda x:x["market"]=="بورس",lowest))[:10]
            for det in lowest:
                name = det['name'] + ' (' + det['name'] + ')'
                top_list.append({
                'name':name,'bestbuyprice':det['3_sell_price'],
                'bestsellprice':det['3_sell_volume'],
                'pricechange':det['close_price_change'],
                'percentchange':det['close_price_change_percent']})

        return top_list

    def bazdehi(self,id):
        det = self.detail(id)
        name = det[0]['name']
        get_data = json.dumps({"Type":"getSymbolFundamentalInfo","la":"Fa","nscCode":id})
        url = 'https://core.tadbirrlc.com//AlmasDataHandler.ashx?'+ get_data + '&jsoncallback='
        bazdehi_det = requests.get(url,timeout=6).json()
        bazdehi_list = {'name':name,'GPE':bazdehi_det['GPE'],'PE':bazdehi_det['PE'],
        'EPS':bazdehi_det['EPS'],'E30':bazdehi_det['E30'],'E90':bazdehi_det['E90'],'E180':bazdehi_det['E180'],
        'E360':bazdehi_det['E360'],'DPSVal':bazdehi_det['DPSVal'],'DPS':bazdehi_det['DPS'],'FYear':bazdehi_det['FYear'],
        'FloatPercent':round(bazdehi_det['FloatPercent'],2),'Valume90AVG':bazdehi_det['Valume90AVG']}
        return bazdehi_list

    def bazdehi_backup(self,id):
        det = self.detail(id)
        name = det[0]['name']
        xname = det[0]['xname']
        url = f'https://sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&&name={xname}'
        bazdehi_det = requests.get(url,timeout=6).json()
        bazdehi_list = {'name':name,'PE':bazdehi_det['P:E'],
        'EPS':bazdehi_det['eps'],'FloatPercent':bazdehi_det['free_float']}
        return bazdehi_list


    def get_sahm_name(self,id):
        get_name = self.detail(id)
        return  get_name[0]['est']

    def stockHolder(self,name):
        try:
            url = f'https://www.sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&stockholder={name}'
            data = requests.get(url).json()
            s = ' '
            text = ''
            for x in  data:
                change = '⁪⁬⬆️'
                if '0' == x['change']:
                    change = ' '
                if '-' in x['change']:
                    change = '⁪⁬⬇️'
                text += f"👤 سهامدار/دارنده : {2*s}{x['name']}\n📈 تغیر : {x['change']} {change}\n📊 درصد‌ : {x['percent']}\n📂 سهم :‌ {x['volume']}\n\n"
        except:
            text = 'مشکل برقراری با api' + '\n\n'
        text += '@DastyarBoursebot'
        return text

    def market(self):
        url_b = 'https://www.sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&market=market_bourse'
        url_f = 'https://www.sourcearena.ir/api/?token=ac3ed71d929300f45b29445f5a844632&market=market_farabourse'
        try:
            bourse = requests.get(url_b,timeout=6).json()['bourse']

            state_b = '🟩 باز'
            if bourse['state'] == 'closed' :
                state_b = '🟥 بسته'

            btext = '💹 بورس' + '\n\n' + '🔰 وضعیت بازار : ' + state_b + '\n\n' + '❎ شاخص کل : ' + bourse['index_change'] + '   ' + bourse['index'] + '\n\n' + \
            '❎ شاخص هم وزن : ' + bourse['index_h'] +  '\n\n' + '📍 ارزش بازار : ' + bourse['market_value'] + '\n\n' + '📍 تعداد معاملات : ' + bourse['trade_number'] + \
            '\n\n' + '📍 حجم معاملات : ' + bourse['trade_volume'] + '\n\n' + '📍 ارزش معاملات : ' + bourse['trade_value']
        except:
            btext = ''


        try:
            farabourse = requests.get(url_f,timeout=6).json()['fara-bourse']

            state_f = '🟩 باز'
            if bourse['state'] == 'closed' :
                state_f = '🟥 بسته'

            ftext = '💹 فرابورس' + '\n\n' + '🔰 وضعیت بازار : ' + state_f + '\n\n' + '❎ شاخص کل : ' + farabourse['index_change'] + '   ' + farabourse['index'] + '\n\n' + \
            '📍 ارزش بازار : ' + farabourse['market_value'] + '\n\n' + '📍 تعداد معاملات : ' + farabourse['trade_number'] + \
            '\n\n' + '📍 حجم معاملات : ' + farabourse['trade_volume'] + '\n\n' + '📍 ارزش معاملات : ' + farabourse['trade_value']
        except:
            ftext = ''

        if len(btext) != 0 or len(ftext) !=0:
            text = btext + '\n\n\n' + ftext + '\n\n' + '@DastyarBoursebot'
        else:
            text = ''
        return text

    def checkfilter(self,text):
        url = 'https://sourcearena.ir/api/filter.php'
        data = {'token':'ac3ed71d929300f45b29445f5a844632','condition':text}
        try:
            req = requests.post(url,json=data,timeout=8).json()
            if 'Error' in req:
                return False
            else:
                return True
        except:
            return False

    def getfilter(self,filter):
        url = 'https://sourcearena.ir/api/filter.php'
        data = {'token':'ac3ed71d929300f45b29445f5a844632','condition':filter}
        try:
            req = requests.post(url,json=data).json()
            return req['result']
        except:
            return None

sahamtool = sahamtool()
#sahamtool.search('پترول')
#print(sahamtool.search('غنوش'))
#sahamtool.detail('IRO1PKOD0001')
#sahamtool.today_top()
#print(sahamtool.bazdehi('IRO1PKOD0001'))
#print(sahamtool.get_sahm_name('IRO1PKOD0001'))
#print(sahamtool.repeater())
